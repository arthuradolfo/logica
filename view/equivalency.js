import * as gen_equivalency from "../js/equivalency_generator.js"
import * as equivalency_questions_view from "./equivalency_questions.js"
import * as keyboard from "../js/keyboard.js"
import * as keyboard_view from "./keyboard.js"
import * as i18n from "../i18n/formula.js"

function generateEquivalencyViews(complexity, difficulty)
{
    document.getElementById("content").innerHTML = ""
    var element = document.createElement("div")
    element.className = "text-left"
    var generated_equivalency = gen_equivalency.generateEquivalency(complexity, difficulty)
    element.innerHTML = "<h3>" + i18n.text_generated_equivalency + generated_equivalency + "</h3>"
    document.getElementById("content").append(element)
    equivalency_questions_view.resetQuestionCounter()
    equivalency_questions_view.nextQuestion()
}

export function showGeneratorButtons()
{
    document.getElementById("facil").addEventListener('click', () => {
        generateEquivalencyViews(1,0)
    })
    document.getElementById("medio").addEventListener('click', () => {
        generateEquivalencyViews(1,1)
    })
    document.getElementById("dificil").addEventListener('click', () => {
        generateEquivalencyViews(2,1)
    })

    keyboard_view.createKeyboard()
    keyboard.getLatestFocus()
}

function createListItem(parent, text)
{
    var li = document.createElement("li")
    li.innerText = text
    parent.append(li)
}

window.onload = function() {
    var subtitle = document.getElementById("subtitle").children
    subtitle[0].innerText = i18n.text_equivalency_title
    subtitle[1].innerText = i18n.text_objective
    subtitle[2].innerText = i18n.text_objective_explain
    subtitle[3].innerText = i18n.text_how_it_works
    subtitle[4].innerText = i18n.text_equivalency_how_it_works_explain
    var description = document.getElementById("description").children
    description[0].innerText = i18n.text_description
    for (var execise of i18n.text_equivalency_exercises)
    {
        createListItem(description[1], execise)
    }
    var generate = document.getElementById("formula").children[2]
    generate.innerText = i18n.text_equivalency_generate_formula
    var buttons = document.getElementById("buttons").children
    buttons[0].innerText = i18n.text_easy
    buttons[1].innerText = i18n.text_medium
    buttons[2].innerText = i18n.text_hard
}