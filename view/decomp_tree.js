import * as i18n from "../i18n/formula.js"
import * as decomp_tree from "../js/decomp_tree.js"
import * as gen_formula from "../js/formula_generator.js"
import * as keyboard from "../js/keyboard.js"
import { isMobile } from "../js/check_mobile.js"
import * as formula_questions_view from "./formula_questions.js"

export function showDecompTree()
{
    var table_element
    var element
    var level = 1
    var sub_formulas_aux = []
    sub_formulas_aux.push(...gen_formula.sub_formulas)
    var sub_formulas_length = sub_formulas_aux.length
    if (sub_formulas_length == 2)
    {
        sub_formulas_aux.pop()
        table_element = document.createElement("table")
        table_element.id = "table"
        var tr = document.createElement("tr")
        tr.append(document.createElement("td"))
        table_element.append(tr)
        element = document.createElement("input")
        element.id = "l1n1"
        if(isMobile) element.readOnly = "readonly"
        table_element.getElementsByTagName("td")[0].append(element)
        document.getElementById("content").append(table_element)
    }
    else
    {
        sub_formulas_aux.pop()
        table_element = document.createElement("table")
        table_element.id = "table"
        var tr = document.createElement("tr")
        tr.append(document.createElement("td"))
        tr.append(document.createElement("td"))
        table_element.append(tr)
        element = document.createElement("input")
        element.id = "l1n1"
        if(isMobile) element.readOnly = "readonly"
        table_element.getElementsByTagName("td")[0].append(element)
        document.getElementById("content").append(table_element)
        element = document.createElement("input")
        element.id = "l1n2"
        if(isMobile) element.readOnly = "readonly"
        table_element.getElementsByTagName("td")[1].append(element)
        document.getElementById("content").append(table_element)
    }
    keyboard.getLatestFocus()
    element = document.createElement("button")
    element.id = "check"
    element.innerHTML = i18n.text_check_answer
    element.onclick = function() { decomp_tree.checkAnswer(level, sub_formulas_aux) }
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    return 0
}

export function updateDecompTree(level, formulas)
{
    document.getElementById("check").remove()
    level += 1
    var table_element
    var element
    var node = 1
    var formulas_aux = []
    for (var i = 0; i < formulas.length; i++)
    {
        var formula = [...formulas[i]]
        if(formula.length == 1)
            continue
        else if (formula.length == 2)
        {
            formula.pop()
            table_element = document.createElement("table")
            var tr = document.createElement("tr")
            tr.append(document.createElement("td"))
            table_element.append(tr)
            element = document.createElement("input")
            element.id = "l" + level + "n" + node
            if(isMobile) element.readOnly = "readonly"
            node+=1
            table_element.getElementsByTagName("td")[0].append(element)
            document.getElementById("l" + (level-1) + "n" + (i+1)).parentNode.append(table_element)
        }
        else
        {
            formula.pop()
            table_element = document.createElement("table")
            var tr = document.createElement("tr")
            tr.append(document.createElement("td"))
            tr.append(document.createElement("td"))
            table_element.append(tr)
            element = document.createElement("input")
            element.id = "l" + level + "n" + node
            if(isMobile) element.readOnly = "readonly"
            node+=1
            table_element.getElementsByTagName("td")[0].append(element)
            document.getElementById("l" + (level-1) + "n" + (i+1)).parentNode.append(table_element)
            element = document.createElement("input")
            element.id = "l" + level + "n" + node
            if(isMobile) element.readOnly = "readonly"
            node+=1
            table_element.getElementsByTagName("td")[1].append(element)
            document.getElementById("l" + (level-1) + "n" + (i+1)).parentNode.append(table_element)
        }
        keyboard.getLatestFocus()
        formulas_aux = [...formulas_aux, ...formula]
    }
    if (formulas_aux.length > 0)
    {
        element = document.createElement("button")
        element.id = "check"
        element.innerHTML = i18n.text_check_answer
        element.onclick = function() { decomp_tree.checkAnswer(level, formulas_aux) }
        document.getElementById("content").append(element)
        element = document.createElement("br")
        document.getElementById("content").append(element)
    }
    else
    {
        formula_questions_view.nextQuestion()
    }
}