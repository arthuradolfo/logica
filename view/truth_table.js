import * as i18n from "../i18n/formula.js";
import * as gen_formula from "../js/formula_generator.js";
import * as gen_set from "../js/set_generator.js";
import * as truth_table from "../js/truth_table.js";
import * as keyboard from "../js/keyboard.js"
import {isMobile} from "../js/check_mobile.js"

export function showTable()
{
    truth_table.evaluateTable()
    var lines = (2**gen_formula.letters_unique.size)+1
    var element = document.createElement("h3")
    element.className = "text-left"
    element.innerText = i18n.text_truth_table
    document.getElementById("content").append(element)
    var table_element = document.createElement("table")
    table_element.id = "table1"
    for (var i = 0; i < lines; i++)
    {
        var tr = document.createElement("tr")
        if(i == 0)
        {
            for (var element of gen_formula.sub_formulas_unique)
            {
                var td = document.createElement("td")
                td.innerHTML = element
                tr.append(td)
            }
        }
        else {
            for (var j = 0; j < gen_formula.sub_formulas_unique.size; j++)
            {
                var td = document.createElement("td")
                var input = document.createElement("input")
                input.id = "l" + (i-1) + "c" + j
                if(isMobile) input.readOnly = "readonly"
                td.append(input)
                tr.append(td)
            }
        }
        table_element.append(tr)
    }
    document.getElementById("content").append(table_element)
    element = document.createElement("br")
    document.getElementById("content").append(element)

    element = document.createElement("button")
    element.id = "checkTable"
    element.innerHTML = i18n.text_check_table
    element.onclick = function() { truth_table.checkTable() }
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showSetTable()
{
    truth_table.evaluateSetTable()
    var lines = (2**gen_set.letters_unique_set.size)+1
    var element = document.createElement("h3")
    element.className = "text-left"
    element.innerText = i18n.text_set_truth_table
    document.getElementById("content").append(element)
    var table_element = document.createElement("table")
    table_element.id = "table1"
    for (var i = 0; i < lines; i++)
    {
        var tr = document.createElement("tr")
        if(i == 0)
        {
            for (var element of gen_set.sub_formulas_unique_set)
            {
                var td = document.createElement("td")
                td.innerHTML = element
                tr.append(td)
            }
        }
        else {
            for (var j = 0; j < gen_set.sub_formulas_unique_set.size; j++)
            {
                var td = document.createElement("td")
                var input = document.createElement("input")
                input.id = "l" + (i-1) + "c" + j
                if(isMobile) input.readOnly = "readonly"
                td.append(input)
                tr.append(td)
            }
        }
        table_element.append(tr)
    }
    document.getElementById("content").append(table_element)
    element = document.createElement("br")
    document.getElementById("content").append(element)

    element = document.createElement("button")
    element.id = "checkTable"
    element.innerHTML = i18n.text_check_table
    element.onclick = function() { truth_table.checkSetTable() }
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showEquivalencyTable()
{
    truth_table.evaluateSetTable()
    var lines = (2**gen_set.letters_unique_set.size)+1
    var element = document.createElement("h3")
    element.className = "text-left"
    element.innerText = i18n.text_equivalency_truth_table
    document.getElementById("content").append(element)
    var table_element = document.createElement("table")
    table_element.id = "table1"
    for (var i = 0; i < lines; i++)
    {
        var tr = document.createElement("tr")
        if(i == 0)
        {
            for (var element of gen_set.sub_formulas_unique_set)
            {
                var td = document.createElement("td")
                td.innerHTML = element
                tr.append(td)
            }
        }
        else {
            for (var j = 0; j < gen_set.sub_formulas_unique_set.size; j++)
            {
                var td = document.createElement("td")
                var input = document.createElement("input")
                input.id = "l" + (i-1) + "c" + j
                if(isMobile) input.readOnly = "readonly"
                td.append(input)
                tr.append(td)
            }
        }
        table_element.append(tr)
    }
    document.getElementById("content").append(table_element)
    element = document.createElement("br")
    document.getElementById("content").append(element)

    element = document.createElement("button")
    element.id = "checkTable"
    element.innerHTML = i18n.text_check_table
    element.onclick = function() { truth_table.checkEquivalencyTable() }
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showInferenceTable()
{
    truth_table.evaluateSetTable()
    var lines = (2**gen_set.letters_unique_set.size)+1
    var element = document.createElement("h3")
    element.className = "text-left"
    element.innerText = i18n.text_inference_truth_table
    document.getElementById("content").append(element)
    var table_element = document.createElement("table")
    table_element.id = "table1"
    for (var i = 0; i < lines; i++)
    {
        var tr = document.createElement("tr")
        if(i == 0)
        {
            for (var element of gen_set.sub_formulas_unique_set)
            {
                var td = document.createElement("td")
                td.innerHTML = element
                tr.append(td)
            }
        }
        else {
            for (var j = 0; j < gen_set.sub_formulas_unique_set.size; j++)
            {
                var td = document.createElement("td")
                var input = document.createElement("input")
                input.id = "l" + (i-1) + "c" + j
                if(isMobile) input.readOnly = "readonly"
                td.append(input)
                tr.append(td)
            }
        }
        table_element.append(tr)
    }
    document.getElementById("content").append(table_element)
    element = document.createElement("br")
    document.getElementById("content").append(element)

    element = document.createElement("button")
    element.id = "checkTable"
    element.innerHTML = i18n.text_check_table
    element.onclick = function() { truth_table.checkInferenceTable() }
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}