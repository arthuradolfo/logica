import * as i18n from "../i18n/formula.js"
import * as keyboard from "../js/keyboard.js"
import {isMobile} from "../js/check_mobile.js"
import * as equivalency_questions from "../js/equivalency_questions.js"
import * as gen_equivalency from "../js/equivalency_generator.js"
import * as truth_table_view from "./truth_table.js"

function generateRadioQuestion(element, id, value, text)
{
    var div = document.createElement("div")
    var input = document.createElement("input")
    input.type = "radio"
    input.id = id
    input.value = value
    input.name = id
    var p = document.createElement("p")
    p.innerText = text
    p.style.display = "inline"
    div.append(input)
    div.append(p)
    element.append(div)
}

export function showEquivalency(button_id)
{
    var element = document.createElement("div")
    element.className = "text-left"
    element.innerHTML = "<h3>" + i18n.text_represents + "</h3>"
    var i = 0
    for (var equivalency of gen_equivalency.equivalencies)
    {
        var random_equivalency = gen_equivalency.randomEquivalencyConnector(i)
        var random_equivalency_inverse = (random_equivalency == i18n.connectors_bin[0]) ? i18n.connectors_bin[1] : i18n.connectors_bin[0]
        generateRadioQuestion(element, "equivalency", i, gen_equivalency.sprintf(equivalency, i18n.phi, i18n.psi, i18n.theta, (gen_equivalency.connector && i == gen_equivalency.equivalency_generated) ? gen_equivalency.connector : random_equivalency, (gen_equivalency.inverse_connector && i == gen_equivalency.equivalency_generated) ? gen_equivalency.inverse_connector : random_equivalency_inverse) + ": " + gen_equivalency.sprintf(i18n.text_equivalencies[i], i18n.phi, i18n.psi, i18n.theta, (gen_equivalency.connector && i == gen_equivalency.equivalency_generated) ? gen_equivalency.connector : random_equivalency, (gen_equivalency.inverse_connector && i == gen_equivalency.equivalency_generated) ? gen_equivalency.inverse_connector : random_equivalency_inverse))
        i+=1
    }
    var div = document.createElement("div")
    var input = document.createElement("button");
    input.className = "margin-left"
    input.id = "checkEquivalency"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { equivalency_questions.checkEquivalency() }
    div.append(input)
    element.append(div)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

function showPossibleEquivalency(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    var generated_equivalency = gen_equivalency.generatePossibleEquivalency(1,0)
    element.innerHTML = "<h3>" + i18n.text_generated_equivalency + generated_equivalency + "</h3>"
    document.getElementById("content").append(element)
    truth_table_view.showEquivalencyTable()
    return 0
}

function generateInputQuestion(element, text, id)
{
    element.className = "text-left"
    element.innerHTML = "<h3>" + text + "</h3>"
    var input = document.createElement("input")
    input.className = "mobile-block mobile-text-input"
    input.id = id
    if(isMobile) input.readOnly = "readonly"
    element.append(input)
}

export function showIsEquivalency(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    element.innerHTML = "<h3>" + i18n.text_is_equivalency + "</h3>"
    generateRadioQuestion(element, "isEquivalency", "yes", i18n.text_yes)
    generateRadioQuestion(element, "isEquivalency", "no", i18n.text_no)
    var div = document.createElement("div")
    var input = document.createElement("button");
    input.className = "margin-left"
    input.id = "checkIsEquivalency"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { equivalency_questions.checkIsEquivalency() }
    div.append(input)
    element.append(div)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showFinish(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("h3")
    element.className = "text-left"
    element.innerHTML = i18n.text_finished
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    return 0
}

export var question_order = [showEquivalency, showPossibleEquivalency, showIsEquivalency, showFinish]
export var current_question = 0

export function nextQuestion(button_id)
{
    var skip = question_order[current_question](button_id)
    current_question+=1
    if(skip == 1)
    {
        question_order[current_question](button_id)
        current_question+=1
    }
}

export function resetQuestionCounter()
{
    current_question = 0
}