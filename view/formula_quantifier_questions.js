import * as i18n from "../i18n/formula.js"
import * as keyboard from "../js/keyboard.js"
import {isMobile} from "../js/check_mobile.js"
import * as formula_quantifier_questions from "../js/formula_quantifier_questions.js"
import * as decomp_tree_quantifier_view from "../view/decomp_tree_quantifier.js"

var sprintf = (str, symbol) => str = str.replaceAll("$", symbol)

function generateRadioQuestion(element, id, value, text)
{
    var div = document.createElement("div")
    var input = document.createElement("input")
    input.type = "radio"
    input.id = id
    input.value = value
    input.name = id
    var p = document.createElement("p")
    p.innerText = text
    p.style.display = "inline"
    div.append(input)
    div.append(p)
    element.append(div)
}

function generateInputQuestion(element, text, id)
{
    element.className = "text-left"
    element.innerHTML = "<h3>" + text + "</h3>"
    var input = document.createElement("input")
    input.className = "mobile-block mobile-text-input"
    input.id = id
    if(isMobile) input.readOnly = "readonly"
    element.append(input)
}

export function showOccurrences(button_id)
{
    if(document.getElementById(button_id) != null) document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    var random_occurrency = formula_quantifier_questions.generateRandomOccurrency()
    generateInputQuestion(element, sprintf(i18n.text_occurrences, random_occurrency), "occurrences")
    var input = document.createElement("button");
    input.id = "checkOccurrency"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_quantifier_questions.checkOccurrences() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showOccurrencesType(button_id)
{
    if(!formula_quantifier_questions.is_variable)
    {
        return 1
    }
    if(document.getElementById(button_id) != null) document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    for (var i = 0; i < formula_quantifier_questions.number_occurrences; i++)
    {
        var h3_element = document.createElement("h3")
        h3_element.innerText = sprintf(i18n.text_occurrences_type, i+1)
        element.append(h3_element)
        generateRadioQuestion(element, "occurrences_type"+i, "connected", i18n.text_connected)
        generateRadioQuestion(element, "occurrences_type"+i, "free", i18n.text_free)
    }
    var input = document.createElement("button");
    input.id = "checkOccurrencesType"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_quantifier_questions.checkOccurrencesType() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showFormulaType(button_id)
{
    if(document.getElementById(button_id) != null) document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    var h3_element = document.createElement("h3")
    h3_element.innerText = i18n.text_formula_type
    element.append(h3_element)
    generateRadioQuestion(element, "formula_type", "open", i18n.text_open)
    generateRadioQuestion(element, "formula_type", "closed", i18n.text_closed)
    var input = document.createElement("button");
    input.id = "checkFormulaType"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_quantifier_questions.checkFormulaType() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showIsSentence(button_id)
{
    if(document.getElementById(button_id) != null) document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    var h3_element = document.createElement("h3")
    h3_element.innerText = i18n.text_sentence
    element.append(h3_element)
    generateRadioQuestion(element, "is_sentence", "yes", i18n.text_yes)
    generateRadioQuestion(element, "is_sentence", "no", i18n.text_no)
    var input = document.createElement("button");
    input.id = "checkIsSentence"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_quantifier_questions.checkIsSentence() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showImediate(button_id)
{
    if(document.getElementById(button_id) != null) document.getElementById(button_id).remove()
    var element = document.createElement("div")
    generateInputQuestion(element, i18n.text_imediate_subformulas, "subform_imed")
    var input = document.createElement("button");
    input.id = "checkImediate"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_quantifier_questions.checkImediates() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showAtomics(button_id)
{
    if(!formula_quantifier_questions.hasAtomic())
    {
        return 1
    }
    if(document.getElementById(button_id) != null) document.getElementById(button_id).remove()
    var element = document.createElement("div")
    generateInputQuestion(element, i18n.text_atomic_subformulas, "subform_atom")
    var input = document.createElement("button");
    input.id = "checkAtomics"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_quantifier_questions.checkAtomics() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showGeneralForm(button_id)
{
    if(!formula_quantifier_questions.hasGeneralsForm())
    {
        return 1
    }
    if(document.getElementById(button_id) != null) document.getElementById(button_id).remove()
    var element = document.createElement("div")
    generateInputQuestion(element, i18n.text_general_form_subformulas, "subform_general")
    var input = document.createElement("button");
    input.id = "checkGeneralForm"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_quantifier_questions.checkGeneralForm() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showGeneralSentece(button_id)
{
    if(!formula_quantifier_questions.hasGeneralsSentence())
    {
        return 1
    }
    if(document.getElementById(button_id) != null) document.getElementById(button_id).remove()
    var element = document.createElement("div")
    generateInputQuestion(element, i18n.text_general_sentence_subformulas, "subform_general_sentence")
    var input = document.createElement("button");
    input.id = "checkGeneralSentence"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_quantifier_questions.checkGeneralSentence() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showNotGeneralSentece(button_id)
{
    if(!formula_quantifier_questions.hasGeneralsNotSentence())
    {
        return 1
    }
    if(document.getElementById(button_id) != null) document.getElementById(button_id).remove()
    var element = document.createElement("div")
    generateInputQuestion(element, i18n.text_not_general_sentence_subformulas, "subform_not_general_sentence")
    var input = document.createElement("button");
    input.id = "checkNotGeneralSentence"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_quantifier_questions.checkNotGeneralSentence() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showMolecular(button_id)
{
    if(!formula_quantifier_questions.hasMolecular())
    {
        return 1
    }
    if(document.getElementById(button_id) != null) document.getElementById(button_id).remove()
    var element = document.createElement("div")
    generateInputQuestion(element, i18n.text_molecular_subformulas, "subform_molecular")
    var input = document.createElement("button");
    input.id = "checkMolecular"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_quantifier_questions.checkMolecular() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showFinish(button_id)
{
    if(document.getElementById(button_id) != null) document.getElementById(button_id).remove()
    var element = document.createElement("h3")
    element.className = "text-left"
    element.innerHTML = i18n.text_finished
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    return 0
}

export var question_order = [decomp_tree_quantifier_view.showDecompTree, showImediate, showAtomics, showGeneralForm, showGeneralSentece, showNotGeneralSentece, showMolecular, showOccurrences, showOccurrencesType, showFormulaType, showIsSentence, showFinish]
export var current_question = 0

export function nextQuestion(button_id)
{
    var skip = question_order[current_question](button_id)
    current_question+=1
    while(skip == 1)
    {
        skip = question_order[current_question](button_id)
        current_question+=1
    }
}

export function resetQuestionCounter()
{
    current_question = 0
}