import * as i18n from "../i18n/arvore_decomp.js"

window.onload = function() {
    var subtitle = document.getElementById("subtitle").children
    subtitle[0].innerText = i18n.text_title
    subtitle[1].innerText = i18n.text_objective
    subtitle[2].innerText = i18n.text_objective_explain
    subtitle[3].innerText = i18n.text_how_it_works
    subtitle[4].innerText = i18n.text_how_it_works_explain
    var description = document.getElementById("description").children
    description[0].innerText = i18n.text_description
    description[1].children[0].innerText = i18n.text_exercises[0]
    description[1].children[1].innerText = i18n.text_exercises[1]
    description[1].children[2].innerText = i18n.text_exercises[2]
    description[1].children[3].innerText = i18n.text_exercises[3]
    description[1].children[4].innerText = i18n.text_exercises[4]
    var generate = document.getElementById("formula").children[2]
    generate.innerText = i18n.text_generate_formula
    var buttons = document.getElementById("buttons").children
    buttons[0].innerText = i18n.text_easy
    buttons[1].innerText = i18n.text_medium
    buttons[2].innerText = i18n.text_hard
}