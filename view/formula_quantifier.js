import * as keyboard_view from "./keyboard.js"
import * as keyboard from "../js/keyboard.js"
import * as i18n from "../i18n/formula.js"
import * as gen_formula_quantifier from "../js/formula_quantifier_generator.js"
import * as formula_quantifier_questions_view from "../view/formula_quantifier_questions.js"

function generateFormulaViews(complexity, difficulty)
{
    document.getElementById("content").innerHTML = ""
    var element = document.createElement("div")
    element.className = "text-left"
    var generated_formula = gen_formula_quantifier.generateFormula(complexity,difficulty)
    element.innerHTML = "<h3>" + i18n.text_generated_formula + generated_formula + "</h3>" + "<h3>" + i18n.text_explain_generated_formula_quantifier + "</h3>" + "<p class='formula-p'>" + generated_formula + "</p>"
    document.getElementById("content").append(element)
    formula_quantifier_questions_view.resetQuestionCounter()
    formula_quantifier_questions_view.nextQuestion()
}

export function showGeneratorButtons()
{
    document.getElementById("facil").addEventListener('click', () => {
        generateFormulaViews(2,0)
    })
    document.getElementById("medio").addEventListener('click', () => {
        generateFormulaViews(2,1)
    })
    document.getElementById("dificil").addEventListener('click', () => {
        generateFormulaViews(3,1)
    })

    keyboard_view.createKeyboardQuantifier()
    keyboard.getLatestFocus()
}

function createListItem(parent, text)
{
    var li = document.createElement("li")
    li.innerText = text
    parent.append(li)
}

window.onload = function() {
    var subtitle = document.getElementById("subtitle").children
    subtitle[0].innerText = i18n.text_title
    subtitle[1].innerText = i18n.text_objective
    subtitle[2].innerText = i18n.text_objective_explain
    subtitle[3].innerText = i18n.text_how_it_works
    subtitle[4].innerText = i18n.text_quantifier_how_it_works_explain
    var description = document.getElementById("description").children
    description[0].innerText = i18n.text_description
    for (var execise of i18n.text_quantifier_exercises)
    {
        createListItem(description[1], execise)
    }
    var generate = document.getElementById("formula_quantifier").children[2]
    generate.innerText = i18n.text_generate_formula
    var buttons = document.getElementById("buttons").children
    buttons[0].innerText = i18n.text_easy
    buttons[1].innerText = i18n.text_medium
    buttons[2].innerText = i18n.text_hard
}