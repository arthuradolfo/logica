import * as i18n from "../i18n/formula.js"
import * as keyboard from "../js/keyboard.js"
import {isMobile} from "../js/check_mobile.js"
import * as formula_questions from "../js/formula_questions.js"
import * as decomp_tree_view from "../view/decomp_tree.js"
import * as truth_table_view from "../view/truth_table.js"

function generateRadioQuestion(element, id, value, text)
{
    var div = document.createElement("div")
    var input = document.createElement("input")
    input.type = "radio"
    input.id = id
    input.value = value
    input.name = id
    var p = document.createElement("p")
    p.innerText = text
    p.style.display = "inline"
    div.append(input)
    div.append(p)
    element.append(div)
}

export function showTautology(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    element.innerHTML = "<h3>" + i18n.text_represents + "</h3>"
    generateRadioQuestion(element, "tautologia", "tautologia", i18n.text_tautology)
    generateRadioQuestion(element, "tautologia", "contradicao", i18n.text_contradiction)
    generateRadioQuestion(element, "tautologia", "contingencia", i18n.text_contingency)
    var div = document.createElement("div")
    var input = document.createElement("button");
    input.className = "margin-left"
    input.id = "checkTautology"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_questions.checkTautology() }
    div.append(input)
    element.append(div)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

function generateInputQuestion(element, text, id)
{
    element.className = "text-left"
    element.innerHTML = "<h3>" + text + "</h3>"
    var input = document.createElement("input")
    input.className = "mobile-block mobile-text-input"
    input.id = id
    if(isMobile) input.readOnly = "readonly"
    element.append(input)
}

export function showImediate(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    generateInputQuestion(element, i18n.text_imediate_subformulas, "subform_imed")
    var input = document.createElement("button");
    input.id = "checkImediate"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_questions.checkImediates() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showAllSubformulas(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    generateInputQuestion(element, i18n.text_all_subformulas, "subform")
    var input = document.createElement("button");
    input.id = "checkSubformulas"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_questions.checkSubformulas() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showFormat(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    element.innerHTML = "<h3>" + i18n.text_format + "</h3>"
    generateRadioQuestion(element, "format", "atomic", i18n.text_format_atomic)
    generateRadioQuestion(element, "format", "negation", i18n.text_format_negation)
    generateRadioQuestion(element, "format", "conjunction", i18n.text_format_conjunction)
    generateRadioQuestion(element, "format", "disjunction", i18n.text_format_disjunction)
    generateRadioQuestion(element, "format", "implication", i18n.text_format_implication)
    generateRadioQuestion(element, "format", "equivalency", i18n.text_format_equivalency)
    var input = document.createElement("button");
    input.id = "checkFormat"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_questions.checkFormat() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showModel(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    element.innerHTML = "<h3>" + i18n.text_model + "</h3>"
    generateRadioQuestion(element, "model", "yes", i18n.text_yes)
    generateRadioQuestion(element, "model", "no", i18n.text_no)
    var input = document.createElement("button");
    input.id = "checkModel"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_questions.checkModel() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showModelLines(button_id)
{
    if (!formula_questions.hasModel())
    {
        return 1
    }
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    generateInputQuestion(element, i18n.text_model_lines, "model_lines")
    var input = document.createElement("button");
    input.id = "checkModelLines"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { formula_questions.checkModelLines() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showFinish(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("h3")
    element.className = "text-left"
    element.innerHTML = i18n.text_finished
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    return 0
}

export var question_order = [decomp_tree_view.showDecompTree, truth_table_view.showTable, showTautology, showImediate, showAllSubformulas, showFormat, showModel, showModelLines, showFinish]
export var current_question = 0

export function nextQuestion(button_id)
{
    var skip = question_order[current_question](button_id)
    current_question+=1
    if(skip == 1)
    {
        question_order[current_question](button_id)
        current_question+=1
    }
}

export function resetQuestionCounter()
{
    current_question = 0
}