import * as gen_inference from "../js/inference_generator.js"
import * as inference_questions_view from "./inference_questions.js"
import * as keyboard from "../js/keyboard.js"
import * as keyboard_view from "./keyboard.js"
import * as i18n from "../i18n/formula.js"

function generateInferenceViews(complexity, difficulty)
{
    document.getElementById("content").innerHTML = ""
    var element = document.createElement("div")
    element.className = "text-left"
    var generated_inference = gen_inference.generateInference(complexity, difficulty)
    element.innerHTML = "<h3>" + i18n.text_generated_inference + generated_inference + "</h3>"
    document.getElementById("content").append(element)
    inference_questions_view.resetQuestionCounter()
    inference_questions_view.nextQuestion()
}

export function showGeneratorButtons()
{
    document.getElementById("facil").addEventListener('click', () => {
        generateInferenceViews(1,0)
    })
    document.getElementById("medio").addEventListener('click', () => {
        generateInferenceViews(1,1)
    })
    document.getElementById("dificil").addEventListener('click', () => {
        generateInferenceViews(2,1)
    })

    keyboard_view.createKeyboard()
    keyboard.getLatestFocus()
}

function createListItem(parent, text)
{
    var li = document.createElement("li")
    li.innerText = text
    parent.append(li)
}

window.onload = function() {
    var subtitle = document.getElementById("subtitle").children
    subtitle[0].innerText = i18n.text_inference_title
    subtitle[1].innerText = i18n.text_objective
    subtitle[2].innerText = i18n.text_objective_explain
    subtitle[3].innerText = i18n.text_how_it_works
    subtitle[4].innerText = i18n.text_inference_how_it_works_explain
    var description = document.getElementById("description").children
    description[0].innerText = i18n.text_description
    for (var execise of i18n.text_inference_exercises)
    {
        createListItem(description[1], execise)
    }
    var generate = document.getElementById("formula").children[2]
    generate.innerText = i18n.text_inference_generate_formula
    var buttons = document.getElementById("buttons").children
    buttons[0].innerText = i18n.text_easy
    buttons[1].innerText = i18n.text_medium
    buttons[2].innerText = i18n.text_hard
}