import * as i18n from "../i18n/formula.js"
import * as keyboard from "../js/keyboard.js"
import {isMobile} from "../js/check_mobile.js"
import * as set_questions from "../js/set_questions.js"
import * as truth_table_view from "./truth_table.js"
import * as implication_view from "./implication.js"

function generateRadioQuestion(element, id, value, text)
{
    var div = document.createElement("div")
    var input = document.createElement("input")
    input.type = "radio"
    input.id = id
    input.value = value
    input.name = id
    var p = document.createElement("p")
    p.innerText = text
    p.style.display = "inline"
    div.append(input)
    div.append(p)
    element.append(div)
}

function generateInputQuestion(element, text, id)
{
    element.className = "text-left"
    element.innerHTML = "<h3>" + text + "</h3>"
    var input = document.createElement("input")
    input.className = "mobile-block mobile-text-input"
    input.id = id
    if(isMobile) input.readOnly = "readonly"
    element.append(input)
}

export function showConsistent(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    element.innerHTML = "<h3>" + i18n.text_is_consistent + "</h3>"
    generateRadioQuestion(element, "consistent", "yes", i18n.text_yes)
    generateRadioQuestion(element, "consistent", "no", i18n.text_no)
    var input = document.createElement("button");
    input.id = "checkConsistent"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { set_questions.checkConsistent() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showModelLines(button_id)
{
    if(!set_questions.isConsistent())
    {
        return 1
    }
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    generateInputQuestion(element, i18n.text_set_model_lines, "model_lines")
    var input = document.createElement("button");
    input.id = "checkModelLines"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { set_questions.checkModelLines() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showIsTautologyImplication(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    element.innerHTML = "<h3>" + i18n.text_set_implication + "</h3>"
    generateRadioQuestion(element, "tautology_implication", "yes", i18n.text_yes)
    generateRadioQuestion(element, "tautology_implication", "no", i18n.text_no)
    var input = document.createElement("button");
    input.id = "checkTautologyImplication"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { set_questions.checkTautologyImplication() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showIsCompatible(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    element.innerHTML = "<h3>" + i18n.text_set_compatible + "</h3>"
    generateRadioQuestion(element, "compatible", "yes", i18n.text_yes)
    generateRadioQuestion(element, "compatible", "no", i18n.text_no)
    var input = document.createElement("button");
    input.id = "checkCompatible"
    input.className = "margin-left mobile-margin-top"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { set_questions.checkCompatible() }
    element.append(input)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showFinish(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("h3")
    element.className = "text-left"
    element.innerHTML = i18n.text_finished
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    return 0
}

export var question_order = [truth_table_view.showSetTable, showConsistent, showModelLines, implication_view.showImplication, showIsTautologyImplication, showIsCompatible, showFinish]
export var current_question = 0

export function nextQuestion(button_id)
{
    var skip = question_order[current_question](button_id)
    current_question+=1
    if(skip == 1)
    {
        question_order[current_question](button_id)
        current_question+=1
    }
}

export function resetQuestionCounter()
{
    current_question = 0
}