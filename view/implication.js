import * as truth_table from "../js/truth_table.js"
import * as gen_set from "../js/set_generator.js"
import * as gen_formula from "../js/formula_generator.js"
import * as i18n from "../i18n/formula.js"
import {isMobile} from "../js/check_mobile.js"
import * as keyboard from "../js/keyboard.js"
import * as implication from "../js/implication.js"

export function showImplication()
{
    var generated_formula = implication.generateImplication()
    var lines = (2**gen_set.letters_unique_set.size)+1
    var element = document.createElement("h3")
    element.className = "text-left"
    element.innerText = i18n.text_set_truth_table+"\n"+generated_formula
    document.getElementById("content").append(element)
    var table_element = document.createElement("table")
    table_element.id = "table1"
    for (var i = 0; i < lines; i++)
    {
        var tr = document.createElement("tr")
        if(i == 0)
        {
            for (var element of gen_set.sub_formulas_unique_set)
            {
                var td = document.createElement("td")
                td.innerHTML = element
                tr.append(td)
            }
            for (var element of gen_formula.sub_formulas_unique)
            {
                if(!gen_set.sub_formulas_unique_set.has(element))
                {
                    var td = document.createElement("td")
                    td.innerHTML = element
                    tr.append(td)
                }
            }
        }
        else {
            for (var j = 0; j < gen_set.sub_formulas_unique_set.size; j++)
            {
                var td = document.createElement("td")
                td.innerHTML = truth_table.valoration[i-1][j] ? i18n.text_truth_capital : i18n.text_false_capital
                tr.append(td)
            }
            var j = 0
            for (var element of gen_formula.sub_formulas_unique)
            {
                if(!gen_set.sub_formulas_unique_set.has(element))
                {
                    var td = document.createElement("td")
                    var input = document.createElement("input")
                    input.id = "li" + (i-1) + "ci" + j
                    if(isMobile) input.readOnly = "readonly"
                    td.append(input)
                    tr.append(td)
                    j += 1
                }
            }
        }
        table_element.append(tr)
    }
    document.getElementById("content").append(table_element)
    element = document.createElement("br")
    document.getElementById("content").append(element)

    element = document.createElement("button")
    element.id = "checkImplication"
    element.innerHTML = i18n.text_check_table
    element.onclick = function() { implication.checkImplication() }
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}