import * as i18n from "../i18n/formula.js"
import * as keyboard from "../js/keyboard.js"
import {isMobile} from "../js/check_mobile.js"
import * as inference_questions from "../js/inference_questions.js"
import * as gen_inference from "../js/inference_generator.js"
import * as truth_table_view from "./truth_table.js"

function generateRadioQuestion(element, id, value, text)
{
    var div = document.createElement("div")
    var input = document.createElement("input")
    input.type = "radio"
    input.id = id
    input.value = value
    input.name = id
    var p = document.createElement("p")
    p.innerText = text
    p.style.display = "inline"
    div.append(input)
    div.append(p)
    element.append(div)
}

export function showInference(button_id)
{
    var element = document.createElement("div")
    element.className = "text-left"
    element.innerHTML = "<h3>" + i18n.text_represents + "</h3>"
    var i = 0
    for (var inference of gen_inference.inferencies_description)
    {
        generateRadioQuestion(element, "inference", i, gen_inference.sprintf(inference, i18n.phi, i18n.psi, i18n.theta, null, null) + ": " + i18n.text_inferencies[i])
        i+=1
    }
    var div = document.createElement("div")
    var input = document.createElement("button");
    input.className = "margin-left"
    input.id = "checkInference"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { inference_questions.checkInference() }
    div.append(input)
    element.append(div)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

function showPossibleInference(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    var generated_inference = gen_inference.generatePossibleInference(1,0)
    element.innerHTML = "<h3>" + i18n.text_generated_inference + generated_inference + "</h3>"
    document.getElementById("content").append(element)
    truth_table_view.showInferenceTable()
    return 0
}

function generateInputQuestion(element, text, id)
{
    element.className = "text-left"
    element.innerHTML = "<h3>" + text + "</h3>"
    var input = document.createElement("input")
    input.className = "mobile-block mobile-text-input"
    input.id = id
    if(isMobile) input.readOnly = "readonly"
    element.append(input)
}

export function showIsInference(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("div")
    element.className = "text-left"
    element.innerHTML = "<h3>" + i18n.text_is_inference + "</h3>"
    generateRadioQuestion(element, "isInference", "yes", i18n.text_yes)
    generateRadioQuestion(element, "isInference", "no", i18n.text_no)
    var div = document.createElement("div")
    var input = document.createElement("button");
    input.className = "margin-left"
    input.id = "checkIsInference"
    input.innerText = i18n.text_check_answer
    input.onclick = function() { inference_questions.checkIsInference() }
    div.append(input)
    element.append(div)
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    keyboard.getLatestFocus()
    return 0
}

export function showFinish(button_id)
{
    document.getElementById(button_id).remove()
    var element = document.createElement("h3")
    element.className = "text-left"
    element.innerHTML = i18n.text_finished
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    return 0
}

export var question_order = [showInference, showPossibleInference, showIsInference, showFinish]
export var current_question = 0

export function nextQuestion(button_id)
{
    var skip = question_order[current_question](button_id)
    current_question+=1
    if(skip == 1)
    {
        question_order[current_question](button_id)
        current_question+=1
    }
}

export function resetQuestionCounter()
{
    current_question = 0
}