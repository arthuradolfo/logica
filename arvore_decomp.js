import * as i18n from "./i18n/arvore_decomp.js";
import * as teclado from "./teclado.js"

var sub_formulas = []
var sub_formulas_unique = new Set
var letters_unique = new Set
var valoracao = []

var isMobile  = false
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ||
    (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.platform))) {
        isMobile = true
}

function randomLetter()
{
    return i18n.letters[Math.floor(Math.random() * 5)]
}

function randomConnector()
{
    var connType = Math.floor(Math.random() * 2)
    if (connType == 0)
    {
        return [i18n.connectors_bin[Math.floor(Math.random() * 4)], true]
    }
    else 
    {
        return [i18n.connectors_sin[0], false]
    }
}

function genLeft(complexity, difficulty)
{
    var sub_sub_formulas = []
    var sub_sub_sub_formulas = []

    var formula = ""
    var formula_aux = ""
    var connector = ""
    var binary = false

    if(complexity == 0)
    {
        var type = Math.floor(Math.random() * 2)
        if (type == 0 || difficulty == 0)
        {
            var letter = randomLetter()
            sub_sub_formulas.push(letter)
            return [letter, sub_sub_formulas]
        }
        else if (type == 1)
        {
            var [connector,binary] = randomConnector()
            if(binary)
            {
                var letter = randomLetter()
                sub_sub_formulas.push([letter])
                formula = "(" + letter
                formula += connector
                letter = randomLetter()
                sub_sub_formulas.push([letter])
                formula += letter
                formula += ")"
                sub_sub_formulas.push(formula)
            }
            else
            {
                var letter = randomLetter()
                sub_sub_formulas.push([letter])
                formula = connector+letter
                sub_sub_formulas.push(formula)
            }
            return [formula, sub_sub_formulas]
        }
        else
        {
            var [connector,binary] = randomConnector()
            if(binary)
            {
                formula = "(" + formula
                formula += connector
                var [formula_aux, sub_sub_sub_formulas] = genLeft(complexity-1, difficulty)
                sub_sub_formulas.push(sub_sub_sub_formulas)
                formula += formula_aux
                formula += ")"
                sub_sub_formulas.push(formula)
            }
            else
            {
                formula = connector+formula
                sub_sub_formulas.push(formula)
            }
            return [formula, sub_sub_formulas]
        }
    }
    
    var[formula, sub_sub_sub_formulas] = genLeft(complexity-1, difficulty)
    sub_sub_formulas.push(sub_sub_sub_formulas)
    var [connector,binary] = randomConnector()
    if(binary)
    {
        formula = "(" + formula
        formula += connector
        var [formula_aux, sub_sub_sub_formulas] = genLeft(complexity-1, difficulty)
        sub_sub_formulas.push(sub_sub_sub_formulas)
        formula += formula_aux
        formula += ")"
        sub_sub_formulas.push(formula)
    }
    else
    {
        formula = connector+formula
        sub_sub_formulas.push(formula)
    }
    return [formula, sub_sub_formulas]
}

function genFormula(complexity, difficulty)
{
    sub_formulas = []
    sub_formulas_unique = new Set
    letters_unique = new Set
    var sub_sub_formulas = []
    if (complexity == 1)
    {
        var [formula, sub_sub_formulas] = genLeft(complexity, difficulty)
        sub_formulas.push(sub_sub_formulas)
    }
    else
    {
        var formula = ""
        var formula_aux = ""
        var connector = ""
        var binary = false
        var [formula,sub_sub_formulas]  = genLeft(complexity-1, difficulty)
        sub_formulas.push(sub_sub_formulas)
        
        var [connector,binary] = randomConnector()
        if(binary)
        {
            formula = "(" + formula
            formula += connector
            var [formula_aux, sub_sub_formulas] = genLeft(complexity-1, difficulty)
            sub_formulas.push(sub_sub_formulas)
            formula += formula_aux
            formula += ")"
            sub_formulas.push(formula)
        }
        else
        {
            formula = connector+formula
            sub_formulas.push(formula)
        }
        return formula
    }
}

function mostrarTabela()
{
    var lines = (2**letters_unique.size)+1
    var element = document.createElement("div")
    element.innerText = i18n.text_tabela_verdade
    document.getElementById("content").append(element)
    var table_element = document.createElement("table")
    table_element.id = "table1"
    for (var i = 0; i < lines; i++)
    {
        var tr = document.createElement("tr")
        if(i == 0)
        {
            for (var element of sub_formulas_unique)
            {
                var td = document.createElement("td")
                td.innerHTML = element
                tr.append(td)
            }
        }
        else {
            for (var j = 0; j < sub_formulas_unique.size; j++)
            {
                var td = document.createElement("td")
                var input = document.createElement("input")
                input.id = "l" + (i-1) + "c" + j
                if(isMobile) input.readOnly = "readonly"
                td.append(input)
                tr.append(td)
            }
        }
        table_element.append(tr)
    }
    document.getElementById("content").append(table_element)
    element = document.createElement("br")
    document.getElementById("content").append(element)

    element = document.createElement("button")
    element.id = "checkTable"
    element.innerHTML = i18n.text_checar_tabela
    element.onclick = function() { checarTabela() }
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)
    teclado.getLatestFocus()
}

function checarResposta(level, formulas)
{
    var table_element
    var errado = false
    if(!Array.isArray(formulas))
        return
    for (var i = 0; i < formulas.length; i++)
    {
        var id = "l" + level + "n" + (i+1)
        var element = document.getElementById(id)
        if (element.value.split(" ").join("") == formulas[i][formulas[i].length-1])
            element.style.border = "2px solid green"
        else
        {   
            element.style.border = "2px solid red"
            errado = true
        }
    }
    if(!errado)
    {
        document.getElementById("check").remove()
        level += 1
        var node = 1
        var formulas_aux = []
        for (var i = 0; i < formulas.length; i++)
        {
            var formula = [...formulas[i]]
            if(formula.length == 1)
                continue
            else if (formula.length == 2)
            {
                formula.pop()
                table_element = document.createElement("table")
                var tr = document.createElement("tr")
                tr.append(document.createElement("td"))
                table_element.append(tr)
                element = document.createElement("input")
                element.id = "l" + level + "n" + node
                if(isMobile) element.readOnly = "readonly"
                node+=1
                table_element.getElementsByTagName("td")[0].append(element)
                document.getElementById("l" + (level-1) + "n" + (i+1)).parentNode.append(table_element)
            }
            else
            {
                formula.pop()
                table_element = document.createElement("table")
                var tr = document.createElement("tr")
                tr.append(document.createElement("td"))
                tr.append(document.createElement("td"))
                table_element.append(tr)
                element = document.createElement("input")
                element.id = "l" + level + "n" + node
                if(isMobile) element.readOnly = "readonly"
                node+=1
                table_element.getElementsByTagName("td")[0].append(element)
                document.getElementById("l" + (level-1) + "n" + (i+1)).parentNode.append(table_element)
                element = document.createElement("input")
                element.id = "l" + level + "n" + node
                if(isMobile) element.readOnly = "readonly"
                node+=1
                table_element.getElementsByTagName("td")[1].append(element)
                document.getElementById("l" + (level-1) + "n" + (i+1)).parentNode.append(table_element)
            }
            teclado.getLatestFocus()
            formulas_aux = [...formulas_aux, ...formula]
        }
        if (formulas_aux.length > 0)
        {
            element = document.createElement("button")
            element.id = "check"
            element.innerHTML = i18n.text_checar_resposta
            element.onclick = function() { checarResposta(level, formulas_aux) }
            document.getElementById("content").append(element)
            element = document.createElement("br")
            document.getElementById("content").append(element)
        }
        else
        {
            mostrarTabela()
        }
    }
}

function findConnector(element)
{
    var open_bracket = 0
    var connector_index = -1;
    var first_element = ""
    var second_element = ""
    var get_element = false
    for (var i = 0; i < element.length; i++)
    {
        if (element[i] == "(")
        {
            open_bracket += 1
            if(open_bracket == 1)
            {
                get_element = true
            }
        }
        else if (element[i] == ")")
        {
            open_bracket -= 1
        }

        if (get_element && connector_index == -1)
        {
            first_element += element[i]
        }
        else if (get_element && connector_index != -1)
        {
            second_element += element[i]
        }

        var index = i18n.connectors_bin.indexOf(element[i])
        if (index >= 0 && open_bracket == 1)
        {
            connector_index = index
        }
    }
    first_element = first_element.slice(1, first_element.length-1)
    second_element = second_element.slice(0, second_element.length-1)
    return [connector_index, first_element, second_element]
}

function isContradicao()
{
    var contradicao = true
    for (var i  = 0; i < valoracao.length; i++)
    {
        if (!valoracao[i][valoracao[i].length-1])
        {
            contradicao = false
        }
    }
    return contradicao
}

function isTautologia()
{
    var tautologia = true
    for (var i  = 0; i < valoracao.length; i++)
    {
        if (!valoracao[i][valoracao[i].length-1])
        {
            tautologia = false
        }
    }
    return tautologia
}

function checarSubformulas()
{
    var certas = 0
    var subformulas = document.getElementById("subform").value.split(" ").join("")
    var subformulas_array = subformulas.split(",")
    var formulas_array = Array.from(sub_formulas_unique)
    if(subformulas_array.length == formulas_array.length)
    {
        for (var i = 0; i < subformulas_array.length; i++)
        {
            if(formulas_array.indexOf(subformulas_array[i]) >= 0)
            {
                certas += 1
            }
        }
    }
    if(certas == subformulas_array.length)
    {
        document.getElementById("subform").style.border = "2px solid green"
        document.getElementById("checarSubformulas").remove()
        var element = document.createElement("div")
        element.innerHTML = i18n.text_terminou
        document.getElementById("content").append(element)
        element = document.createElement("br")
        document.getElementById("content").append(element)
        element = document.createElement("br")
        document.getElementById("content").append(element)
    }
    else
    {
        document.getElementById("subform").style.border = "2px solid red"
    }
}

function checarImediatas()
{
    var certas = 0
    var imediatas = document.getElementById("subform_imed").value.split(" ").join("")
    var imediatas_array = imediatas.split(",")
    if(imediatas_array.length == sub_formulas.length-1)
    {
        for (var i = 0; i < imediatas_array.length; i++)
        {
            if(sub_formulas[i].indexOf(imediatas_array[i]) >= 0)
            {
                certas += 1
            }
        }
    }
    if(certas == imediatas_array.length)
    {
        document.getElementById("subform_imed").style.border = "2px solid green"
        document.getElementById("checarImediatas").remove()
        var element = document.createElement("div")
        element.innerHTML = i18n.text_todas_subformulas
        var input = document.createElement("input")
        if(isMobile) input.readOnly = "readonly"
        input.id = "subform"
        element.append(input)
        input = document.createElement("button");
        input.id = "checarSubformulas"
        input.innerText = i18n.text_checar_resposta
        input.onclick = function() { checarSubformulas() }
        element.append(input)
        document.getElementById("content").append(element)
        element = document.createElement("br")
        document.getElementById("content").append(element)
        element = document.createElement("br")
        document.getElementById("content").append(element)
        teclado.getLatestFocus()
    }
    else
    {
        document.getElementById("subform_imed").style.border = "2px solid red"
    }
}

function checarTautologia()
{
    var elements = document.getElementsByName("tautologia")
    var certo = false
    for (element of elements)
    {
        if (element.value == "tautologia" && element.checked && isTautologia())
        {
            element.nextSibling.style.border = "2px solid green"
            certo = true
        }
        else if (element.value == "contradicao" && element.checked && isContradicao())
        {
            element.nextSibling.style.border = "2px solid green"
            certo = true
        }
        else if (element.value == "contingencia" && element.checked && (!isContradicao() && !isTautologia()))
        {
            element.nextSibling.style.border = "2px solid green"
            certo = true
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
    if (certo)
    {
        document.getElementById("checarTautologia").remove()
        var element = document.createElement("div")
        element.innerHTML = i18n.text_subformulas_imediatas
        var input = document.createElement("input")
        input.id = "subform_imed"
        if(isMobile) input.readOnly = "readonly"
        element.append(input)
        input = document.createElement("button");
        input.id = "checarImediatas"
        input.innerText = i18n.text_checar_resposta
        input.onclick = function() { checarImediatas() }
        element.append(input)
        document.getElementById("content").append(element)
        element = document.createElement("br")
        document.getElementById("content").append(element)
        element = document.createElement("br")
        document.getElementById("content").append(element)
        teclado.getLatestFocus()
    }
}

function checarTabela()
{
    var lines = (2**letters_unique.size)
    valoracao = []
    for (var i = 0; i < lines; i++)
    {
        valoracao.push([])
        var j = letters_unique.size-1;
        for (var element of sub_formulas_unique)
        {
            if (letters_unique.has(element))
            {
                if (((i >> j) & 0x1) == 0)
                    valoracao[i].push(true)
                else
                    valoracao[i].push(false)

                j--
            }
            else
            {
                if (element[0] == i18n.connectors_sin[0])
                {
                    var element_aux = element.slice(1)
                    var formulas_array = Array.from(sub_formulas_unique)
                    var index = formulas_array.indexOf(element_aux)
                    if (index >= 0)
                    {
                        valoracao[i].push(!valoracao[i][index])
                    }
                }
                else if (element[0] == "(")
                {
                    var [connector, first_element, second_element] = findConnector(element)
                    if (connector == 0)
                    {
                        var element_aux = element.slice(1)
                        var formulas_array = Array.from(sub_formulas_unique)
                        var first_index = formulas_array.indexOf(first_element)
                        var second_index = formulas_array.indexOf(second_element)
                        valoracao[i].push(valoracao[i][first_index] && valoracao[i][second_index])
                    }
                    else if (connector == 1)
                    {
                        var element_aux = element.slice(1)
                        var formulas_array = Array.from(sub_formulas_unique)
                        var first_index = formulas_array.indexOf(first_element)
                        var second_index = formulas_array.indexOf(second_element)
                        valoracao[i].push(valoracao[i][first_index] || valoracao[i][second_index])
                    }
                    else if (connector == 2)
                    {
                        var element_aux = element.slice(1)
                        var formulas_array = Array.from(sub_formulas_unique)
                        var first_index = formulas_array.indexOf(first_element)
                        var second_index = formulas_array.indexOf(second_element)
                        valoracao[i].push(!valoracao[i][first_index] || valoracao[i][second_index])
                    }
                    else if (connector == 3)
                    {
                        var element_aux = element.slice(1)
                        var formulas_array = Array.from(sub_formulas_unique)
                        var first_index = formulas_array.indexOf(first_element)
                        var second_index = formulas_array.indexOf(second_element)
                        valoracao[i].push(valoracao[i][first_index] == valoracao[i][second_index])
                    }
                }
            }
        }
    }
    var errado = false
    for (var i = 0; i < lines; i++)
    {
        for (var j = 0; j < sub_formulas_unique.size; j++)
        {
            var element = document.getElementById("l" + i + "c" + j);
            if (((element.value == i18n.text_verdadeiro_maiusculo || element.value == i18n.text_verdadeiro_minusculo) && valoracao[i][j]) 
                || ((element.value == i18n.text_falso_maiusculo || element.value == i18n.text_falso_minusculo) && !valoracao[i][j]))
            {
                element.style.border = "2px solid green"
            }
            else
            {
                errado = true
                element.style.border = "2px solid red"
            }
        }
    }
    if(!errado)
    {
        document.getElementById("checkTable").remove()
        var element = document.createElement("div")
        element.innerHTML = i18n.text_representa
        var input = document.createElement("input")
        input.type = "radio"
        input.id = "tautologia"
        input.value = "tautologia"
        input.name = "tautologia"
        var p = document.createElement("p")
        p.innerText = i18n.text_tautologia
        p.style.display = "inline"
        element.append(input)
        element.append(p)
        input = document.createElement("input")
        input.type = "radio"
        input.id = "tautologia"
        input.value = "contradicao"
        input.name = "tautologia"
        p = document.createElement("p")
        p.innerText = i18n.text_contradicao
        p.style.display = "inline"
        element.append(input)
        element.append(p)
        input = document.createElement("input")
        input.type = "radio"
        input.id = "tautologia"
        input.value = "contingencia"
        input.name = "tautologia"
        p = document.createElement("p")
        p.innerText = i18n.text_contingencia
        p.style.display = "inline"
        element.append(input)
        element.append(p)
        input = document.createElement("button");
        input.id = "checarTautologia"
        input.innerText = i18n.text_checar_resposta
        input.onclick = function() { checarTautologia() }
        element.append(input)
        document.getElementById("content").append(element)
        element = document.createElement("br")
        document.getElementById("content").append(element)
        element = document.createElement("br")
        document.getElementById("content").append(element)
        teclado.getLatestFocus()
    }
}

function generateFormula(complexity, difficulty)
{
    document.getElementById("content").innerHTML = ""
    var element = document.createElement("div")
    var table_element
    var formula_gerada = genFormula(complexity, difficulty)
    element.innerHTML = i18n.text_formula_gerada + formula_gerada + i18n.text_explain_formula_gerada + formula_gerada
    document.getElementById("content").append(element)
    var level = 1
    var sub_formulas_aux = []
    sub_formulas_aux.push(...sub_formulas)
    var sub_formulas_length = sub_formulas_aux.length
    if (sub_formulas_length == 2)
    {
        sub_formulas_aux.pop()
        table_element = document.createElement("table")
        table_element.id = "table"
        var tr = document.createElement("tr")
        tr.append(document.createElement("td"))
        table_element.append(tr)
        element = document.createElement("input")
        element.id = "l1n1"
        if(isMobile) element.readOnly = "readonly"
        table_element.getElementsByTagName("td")[0].append(element)
        document.getElementById("content").append(table_element)
    }
    else
    {
        sub_formulas_aux.pop()
        table_element = document.createElement("table")
        table_element.id = "table"
        var tr = document.createElement("tr")
        tr.append(document.createElement("td"))
        tr.append(document.createElement("td"))
        table_element.append(tr)
        element = document.createElement("input")
        element.id = "l1n1"
        if(isMobile) element.readOnly = "readonly"
        table_element.getElementsByTagName("td")[0].append(element)
        document.getElementById("content").append(table_element)
        element = document.createElement("input")
        element.id = "l1n2"
        if(isMobile) element.readOnly = "readonly"
        table_element.getElementsByTagName("td")[1].append(element)
        document.getElementById("content").append(table_element)
    }
    teclado.getLatestFocus()
    element = document.createElement("button")
    element.id = "check"
    element.innerHTML = "Checar Resposta"
    element.onclick = function() { checarResposta(level, sub_formulas_aux) }
    document.getElementById("content").append(element)
    element = document.createElement("br")
    document.getElementById("content").append(element)

    var sub_formulas_flat = sub_formulas.flat(100)
    var sub_formulas_flat_sort = sub_formulas_flat.sort((a,b) => a.length - b.length);
    for (var i = 0; i < sub_formulas_flat_sort.length; i++)
    {
        sub_formulas_unique.add(sub_formulas_flat_sort[i])
        if(sub_formulas_flat_sort[i].length == 1)
            letters_unique.add(sub_formulas_flat_sort[i])
    }
}

document.getElementById("facil").addEventListener('click', () => {
    generateFormula(2,0)
})
document.getElementById("medio").addEventListener('click', () => {
    generateFormula(2,1)
})
document.getElementById("dificil").addEventListener('click', () => {
    generateFormula(3,1)
})

teclado.createTecladoButtons()
teclado.getLatestFocus()