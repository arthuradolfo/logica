export const letters = ["A", "B", "C", "D", "E"]
export const connectors_bin = ["∧", "∨", "→", "↔"]
export const connectors_sin = ["¬"]
export const parenthesis = ["(", ")"]
export const table = ["V", "F"]
export const backspace = "Deletar"
export const comma = ","
export const text_generated_formula = "Fórmula gerada: "
export const text_explain_generated_formula = "Faça a árvore de decomposição, linha por linha, abaixo. Utilize os mesmos símbolos para os conectivos.<br>"
export const text_represents = "A fórmula acima representa: "
export const text_tautology = "Tautologia"
export const text_contradiction = "Contradição"
export const text_contingency = "Contingência"
export const text_check_answer = "Checar Resposta"
export const text_truth_capital = "V"
export const text_truth_lower = "v"
export const text_false_capital = "F"
export const text_false_lower = "f"
export const text_imediate_subformulas = "Quais as subformulas imediatas da fórmula acima? (Responda separando as subfórmulas por vírgulas. Ex: A,B): "
export const text_all_subformulas = "Liste todas as subfórmulas da fórmula acima (Responda separando as subfórmulas por vírgulas e sem espaço. Ex: A,B): "
export const text_finished = "Parabéns! Você terminou as questões referentes a essa fórmula. Gere uma nova clicando nos botões 'Fácil', 'Médio', 'Difícil' acima."
export const text_check_table = "Checar Tabela"
export const text_truth_table = "Faça a tabela verdade da fórmula gerada. Lembre-se de utilizar a regra aprendida em aula para preencher as colunas das letras sentenciais. Pode utilizar " + text_truth_capital + ", " + text_truth_lower + ", " + text_false_capital + " ou " + text_false_lower + "."
export const text_title = "Gerador de fórmulas da Linguagem L"
export const text_objective = "Objetivo do programa:"
export const text_objective_explain = "Este programa foi feito para auxiliar o estudo da Linguagem L, apresentada na disciplina de Lógica I do curso de Filosofia do IFCH-UFRGS"
export const text_how_it_works = "Funcionamento:"
export const text_how_it_works_explain = "Uma fórmula aleatória é gerada e o usuário precisa fazer a árvore de decomposição, a tabela verdade e responder a algumas perguntas sobre a fórmula gerada."
export const text_description = "Exercícios contidos:"
export const text_exercises = ["Fazer árvore de decomposição", "Completar tabela verdade", "Identificar se é tautologia, contradição ou contingência", "Listar todas subfórumulas imediatas", "Listar todas subfórmulas"]
export const text_teclado_explain = "Teclado virtual: Selecione uma caixa de texto e clique no botão desejado para adicionar o caractere!"
export const text_generate_formula = "Gere uma fórmula clicando nos botões abaixo, escolhendo a dificuldade."
export const text_easy = "Fácil"
export const text_medium = "Médio"
export const text_hard = "Difícil"