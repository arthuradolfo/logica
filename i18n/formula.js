// Symbols
export const letters = ["A", "B", "C", "D", "E"]
export const numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
export const connectors_bin = ["∧", "∨", "→", "↔"]
export const connectors_sin = ["¬"]
export const parenthesis = ["(", ")"]
export const table = ["V", "F"]
export const backspace = "Deletar"
export const comma = ","
export const phi = "Φ"
export const psi = "Ψ"
export const theta = "θ"
export const variables = ["u", "v", "x", "y", "z"]
export const constants = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t"]
export const predicates = ["A&", "B&", "C&", "D&", "E&"]
export const quantifiers = ["∀", "∃"]
export const auxiliar = ","
export const grades = ["¹", "²", "³"]
export const break_line = "<br>"
export const horizontal_line = "<hr>"
export const inference = "|="
export const constants_keyboard = "Constantes"
export const go_back = "Voltar"
// Formats
export const text_format_atomic = "Π"
export const text_format_negation = "¬Φ"
export const text_format_conjunction = "(Φ∧Ψ)"
export const text_format_disjunction = "(Φ∨Ψ)"
export const text_format_implication = "(Φ→Ψ)"
export const text_format_equivalency = "(Φ↔Ψ)"
// Equivalencies
export const text_equivalencies = [
    "Dupla negação",
    "Definição de " + connectors_bin[2] + " por " + connectors_sin[0] + " e " + connectors_bin[1],
    "Contração da conjunção",
    "Comutação para _",
    "De Morgan",
    "Associação para _",
    "Contraposição",
    "Equivalência entre _ e !",
    "Distributiva",
    "Exportação/Importação",
    "Definição de " + connectors_bin[2] + " por " + connectors_sin[0] + " e " + connectors_bin[0],
    "Definição de " + connectors_bin[0] + " por " + connectors_sin[0] + " e " + connectors_bin[2],
    "Definição de " + connectors_bin[1] + " por " + connectors_sin[0] + " e " + connectors_bin[2],
    "Absorção",
    "",
    "",
    "",
    "",
    ""
]
export const text_inferencies = [
    "Modus Ponens",
    "Modus Tollens",
    "Pré-Fixação",
    "Silogismos Hipotéticos",
    "Silogismo Disjuntivo",
    "Duns Scotus",
    "Prova por casos",
    "Eliminação do segundo conjuntivo",
    "Eliminação do primeiro conjuntivo",
    "Introdução do segundo disjuntivo",
    "Introdução do primeiro disjuntivo",
    "Lei de Redução ao Absurdo"
]
// Descriptions
export const text_title = "Gerador de fórmulas da Linguagem L"
export const text_set_title = "Gerador de conjunto de fórmulas da Linguagem L"
export const text_equivalency_title = "Gerador de equivalências tautológicas da Linguagem L"
export const text_inference_title = "Gerador de inferências tautológicas da Linguagem L"
export const text_quantifier_title = "Gerador de fórmulas quantificacionais da Linguagem L"
export const text_objective = "Objetivo do programa:"
export const text_objective_explain = "Este programa foi feito para auxiliar o estudo da Linguagem L, apresentada na disciplina de Lógica I do curso de Filosofia do IFCH-UFRGS"
export const text_how_it_works = "Funcionamento:"
export const text_how_it_works_explain = "Uma fórmula aleatória é gerada e o usuário precisa fazer a árvore de decomposição, a tabela verdade e responder a algumas perguntas sobre a fórmula gerada."
export const text_set_how_it_works_explain = "Um conjunto de fórmulas aleatório é gerado e o usuário precisa fazer a tabela verdade e responder a algumas perguntas sobre o conjunto gerado."
export const text_equivalency_how_it_works_explain = "Uma fórmula aleatória que representa uma equivalência tautológica é gerada e o usuário precisa indicar o esquema tautológico. Após, outras duas fórmulas são geradas e apresentadas em forma de equivalência e o usuário deverá fazer a tabela verdade e decidir se a equivalência em questão é uma equivalência tautológica."
export const text_quantifier_how_it_works_explain = "Uma fórmula aleatória é gerada e o usuário precisa fazer a árvore de decomposição, a tabela verdade e responder a algumas perguntas sobre a fórmula gerada."
export const text_inference_how_it_works_explain = "Uma inferência tautológica é gerada e o usuário precisa indicar a forma da inferência."
export const text_description = "Exercícios contidos:"
export const text_exercises = ["Fazer árvore de decomposição", "Completar tabela verdade", "Identificar se é tautologia, contradição ou contingência", "Listar todas subfórumulas imediatas", "Listar todas subfórmulas", "Indicar a forma", "Indicar, se existir, valorações modelo da fórmula"]
export const text_set_exercises = ["Completar tabela verdade", "Identificar se conjunto é consistente"]
export const text_equivalency_exercises = ["Identificar o esquema tautológico", "Completar tabela verdade", "Identificar se é equivalência tautológica"]
export const text_quantifier_exercises = ["Fazer árvore de decomposição", "Completar tabela verdade", "Identificar se é tautologia, contradição ou contingência", "Listar todas subfórumulas imediatas", "Listar todas subfórmulas", "Indicar a forma", "Indicar, se existir, valorações modelo da fórmula"]
export const text_inference_exercises = ["Identificar a inferência tautológica", "Fazer a tabela verdade de uma inferência tautológica"]
export const text_generated_formula = "Fórmula gerada: "
export const text_generate_formula = "Gere uma fórmula clicando nos botões abaixo, escolhendo a dificuldade."
export const text_generated_set = "Conjunto de fórmulas gerado: "
export const text_set_generate_formula = "Gere um conjunto de fórmulas clicando nos botões abaixo, escolhendo a dificuldade."
export const text_generated_equivalency = "Fórmula gerada: "
export const text_equivalency_generate_formula = "Gere uma fórmula clicando nos botões abaixo, escolhendo a dificuldade."
export const text_generated_inference = "Inferência gerada: <br>"
export const text_inference_generate_formula = "Gere uma inferência tautológica clicando nos botões abaixo, escolhendo a dificuldade."
export const text_easy = "Fácil"
export const text_medium = "Médio"
export const text_hard = "Difícil"
// Decomp tree
export const text_explain_generated_formula = "Faça a árvore de decomposição, linha por linha, abaixo. Utilize os mesmos símbolos para os conectivos.<br>"
export const text_explain_generated_formula_quantifier = "Faça a árvore de decomposição, linha por linha, abaixo. Utilize os mesmos símbolos para os conectivos.<br>"
// Truth Table
export const text_truth_capital = "V"
export const text_truth_lower = "v"
export const text_false_capital = "F"
export const text_false_lower = "f"
export const text_truth_table = "Faça a tabela verdade da fórmula gerada. Lembre-se de utilizar a regra aprendida em aula para preencher as colunas das letras sentenciais. Pode utilizar " + text_truth_capital + ", " + text_truth_lower + ", " + text_false_capital + " ou " + text_false_lower + "."
export const text_set_truth_table = "Faça a tabela verdade do conjunto gerado. Lembre-se de utilizar a regra aprendida em aula para preencher as colunas das letras sentenciais. Pode utilizar " + text_truth_capital + ", " + text_truth_lower + ", " + text_false_capital + " ou " + text_false_lower + "."
export const text_equivalency_truth_table = "Faça a tabela verdade da possível equivalência gerada acima. Lembre-se de utilizar a regra aprendida em aula para preencher as colunas das letras sentenciais. Pode utilizar " + text_truth_capital + ", " + text_truth_lower + ", " + text_false_capital + " ou " + text_false_lower + "."
export const text_inference_truth_table = "Faça a tabela verdade da possível inferência tautológica gerada acima. Lembre-se de utilizar a regra aprendida em aula para preencher as colunas das letras sentenciais. Pode utilizar " + text_truth_capital + ", " + text_truth_lower + ", " + text_false_capital + " ou " + text_false_lower + "."
export const text_check_table = "Checar Tabela"
// Implication
export const text_set_truth_table_implication = "Faça a tabela verdade da formula a seguir com o conjunto gerado acima. Lembre-se de utilizar a regra aprendida em aula para preencher as colunas das letras sentenciais. Pode utilizar " + text_truth_capital + ", " + text_truth_lower + ", " + text_false_capital + " ou " + text_false_lower + "."
export const text_set_implication = "A formula gerada é consequência tautológica do conjunto gerado acima?"
export const text_set_compatible = "A formula gerada é proposicionalmente compatível com o conjunto gerado acima?"
// Formula Questions
export const text_represents = "A fórmula acima representa: "
export const text_tautology = "Tautologia"
export const text_contradiction = "Contradição"
export const text_contingency = "Contingência"
export const text_imediate_subformulas = "Quais as subformulas imediatas da fórmula acima? (Responda separando as subfórmulas por vírgulas. Ex: A,B): "
export const text_all_subformulas = "Liste todas as subfórmulas da fórmula acima (Responda separando as subfórmulas por vírgulas e sem espaço. Ex: A,B): "
export const text_format = "Qual a forma da fórmula gerada acima? "
export const text_model = "A fórmula acima possui valoração modelo? "
export const text_yes = "Sim"
export const text_no = "Não"
export const text_model_lines = "Indique as linhas que são valoração modelo da fórmula: "
export const text_finished = "Parabéns! Você terminou as questões referentes a essa fórmula. Gere uma nova clicando nos botões 'Fácil', 'Médio', 'Difícil' acima."
export const text_check_answer = "Checar Resposta"
// Set Questions
export const text_is_consistent = "O conjunto de fórmulas gerado acima é consistente?"
export const text_set_model_lines = "Indique as linhas que são valoração modelo do conjunto: "
// Equivalency Questions
export const text_is_equivalency = "A fórmula acima é uma equivalência tautológica?"
// Inference Questions
export const text_is_inference = "A fórmula acima é uma inferência tautológica?"
// Formula Quantifier Question
export const text_occurrences = "Quantas ocorrências o símbolo '$' possui?"
export const text_occurrences_type = "Qual o tipo da ocorrência #$?"
export const text_connected = "Ligada"
export const text_free = "Livre"
export const text_formula_type = "Qual o tipo da fórmula gerada acima?"
export const text_open = "Aberta"
export const text_closed = "Fechada"
export const text_sentence = "A fórmula gerada acima é uma sentença?"
export const text_atomic_subformulas = "Quais as subformulas atômicas da fórmula acima? (Responda separando as subfórmulas por vírgulas. Ex: A,B): "
export const text_general_form_subformulas = "Quais as subformulas que são fórmulas gerais da fórmula acima? (Responda separando as subfórmulas por vírgulas. Ex: A,B): "
export const text_general_sentence_subformulas = "Quais as subformulas que são sentenças gerais da fórmula acima? (Responda separando as subfórmulas por vírgulas. Ex: A,B): "
export const text_not_general_sentence_subformulas = "Quais as subformulas que são sentenças não gerais da fórmula acima? (Responda separando as subfórmulas por vírgulas. Ex: A,B): "
export const text_molecular_subformulas = "Quais as subformulas moleculares da fórmula acima? (Responda separando as subfórmulas por vírgulas. Ex: A,B): "
// Teclado
export const text_teclado_explain = "Teclado virtual: Selecione uma caixa de texto e clique no botão desejado para adicionar o caractere!"