# Como executar este programa

- Baixe o projeto: `git clone https://gitlab.com/arthuradolfo/logica.git`
- Acesse a pasta raiz do projeto: `cd logica`
- Rode o programa com: `python3 arvore_serve.py`
- Acesse localhost:8001 em um navegador para verificar se o programa rodou corretamente.
