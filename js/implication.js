import * as set_questions_view from "../view/set_questions.js"
import * as gen_formula from "./formula_generator.js"
import * as gen_set from "./set_generator.js"
import * as i18n from "../i18n/formula.js"
import * as parser from "./formula_parser.js"
import * as truth_table from "../js/truth_table.js"

export var implicated_table = []

function getElementIndex(element)
{
    var element_aux = element.slice(1)
    if(gen_set.sub_formulas_unique_set.has(element_aux))
    {
        return -1
    }
    var formulas_array = Array.from(gen_formula.sub_formulas_unique)
    return formulas_array.indexOf(element_aux) 
}

function getElementsIndexes(first_element, second_element)
{
    var first_index, second_index
    var formulas_array = Array.from(gen_formula.sub_formulas_unique)
    if(gen_set.sub_formulas_unique_set.has(first_element))
    {
        first_index = -1
    }
    else
    {
        first_index = formulas_array.indexOf(first_element)
    }
    if(gen_set.sub_formulas_unique_set.has(second_element))
    {
        second_index = -1
    }
    else
    {
        second_index = formulas_array.indexOf(second_element)
    }

    return [first_index, second_index]
}

function getSetElementIndex(element)
{
    var element_aux = element.slice(1)
    return gen_set.sub_formulas_set.indexOf(element_aux)
}

function getSetElementsIndexes(first_element, second_element)
{
    return [gen_set.sub_formulas_set.indexOf(first_element), gen_set.sub_formulas_set.indexOf(second_element)]
}

export function evaluateTable()
{
    var lines = (2**gen_set.letters_unique_set.size)
    implicated_table = []
    for (var i = 0; i < lines; i++)
    {
        implicated_table.push([])
        for (var element of gen_formula.sub_formulas_unique)
        {
            if (element[0] == i18n.connectors_sin[0])
            {
                var index = getElementIndex(element)
                if (index >= 0)
                {
                    implicated_table[i].push(!implicated_table[i][index])
                }
                else
                {
                    var index = getSetElementIndex(element)
                    if (index >= 0)
                    {
                        implicated_table[i].push(!truth_table.valoration[i][index])
                    }
                }
            }
            else if (element[0] == "(")
            {
                var [connector, first_element, second_element] = parser.findConnector(element)
                if (connector == 0)
                {
                    var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                    var first_valoration = true, second_valoration = true
                    if(first_index >= 0)
                    {
                        first_valoration = implicated_table[i][first_index]
                    }
                    else
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        if(first_index >= 0)
                        {
                            first_valoration = truth_table.valoration[i][first_index]
                        }
                    }
                    var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                    if(second_index >= 0)
                    {
                        second_valoration = implicated_table[i][second_index]
                    }
                    else
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        if(second_index >= 0)
                        {
                            second_valoration = truth_table.valoration[i][second_index]
                        }
                    }
                    implicated_table[i].push(first_valoration && second_valoration)
                }
                else if (connector == 1)
                {
                    var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                    var first_valoration = true, second_valoration = true
                    if(first_index >= 0)
                    {
                        first_valoration = implicated_table[i][first_index]
                    }
                    else
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        if(first_index >= 0)
                        {
                            first_valoration = truth_table.valoration[i][first_index]
                        }
                    }
                    var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                    if(second_index >= 0)
                    {
                        second_valoration = implicated_table[i][second_index]
                    }
                    else
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        if(second_index >= 0)
                        {
                            second_valoration = truth_table.valoration[i][second_index]
                        }
                    }
                    implicated_table[i].push(first_valoration || second_valoration)
                }
                else if (connector == 2)
                {
                    var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                    var first_valoration = true, second_valoration = true
                    if(first_index >= 0)
                    {
                        first_valoration = implicated_table[i][first_index]
                    }
                    else
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        if(first_index >= 0)
                        {
                            first_valoration = truth_table.valoration[i][first_index]
                        }
                    }
                    var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                    if(second_index >= 0)
                    {
                        second_valoration = implicated_table[i][second_index]
                    }
                    else
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        if(second_index >= 0)
                        {
                            second_valoration = truth_table.valoration[i][second_index]
                        }
                    }
                    implicated_table[i].push(!first_valoration || second_valoration)
                }
                else if (connector == 3)
                {
                    var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                    var first_valoration = true, second_valoration = true
                    if(first_index >= 0)
                    {
                        first_valoration = implicated_table[i][first_index]
                    }
                    else
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        if(first_index >= 0)
                        {
                            first_valoration = truth_table.valoration[i][first_index]
                        }
                    }
                    var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                    if(second_index >= 0)
                    {
                        second_valoration = implicated_table[i][second_index]
                    }
                    else
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        if(second_index >= 0)
                        {
                            second_valoration = truth_table.valoration[i][second_index]
                        }
                    }
                    implicated_table[i].push(first_valoration == second_valoration)
                }
            }
        }
    }
}

export function checkImplication()
{
    var lines = (2**gen_set.letters_unique_set.size)
    var wrong = false
    for (var i = 0; i < lines; i++)
    {
        for (var j = 0; j < gen_formula.sub_formulas_unique.size; j++)
        {
            var element = document.getElementById("li" + i + "ci" + j);
            if (((element.value == i18n.text_truth_capital || element.value == i18n.text_truth_lower) && implicated_table[i][j]) 
                || ((element.value == i18n.text_false_capital || element.value == i18n.text_false_lower) && !implicated_table[i][j]))
            {
                element.style.border = "2px solid green"
            }
            else
            {
                wrong = true
                element.style.border = "2px solid red"
            }
        }
    }
    if(!wrong)
    {
        set_questions_view.nextQuestion("checkImplication")
    }
}

export function generateImplication()
{
    var formula_gerada = gen_formula.generateFormula(2,0, Array.from(gen_set.letters_unique_set))
    while(gen_set.set.indexOf(formula_gerada) >= 0)
    {
        formula_gerada = gen_formula.generateFormula(2,0, Array.from(gen_set.letters_unique_set))
    }
    gen_formula.removeFormulasFromArray(gen_set.sub_formulas_unique_set)
    evaluateTable()
    return formula_gerada;
}