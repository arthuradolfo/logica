import * as formula_questions from "../view/formula_questions.js"
import * as set_questions_view from "../view/set_questions.js"
import * as equivalency_questions_view from "../view/equivalency_questions.js"
import * as inference_questions_view from "../view/inference_questions.js"
import * as gen_formula from "./formula_generator.js"
import * as gen_set from "./set_generator.js"
import * as i18n from "../i18n/formula.js"
import * as parser from "./formula_parser.js"

export var valoration = []
export var models = []

function getElementIndex(element)
{
    var element_aux = element.slice(1)
    var formulas_array = Array.from(gen_formula.sub_formulas_unique)
    return formulas_array.indexOf(element_aux)
}

function getElementsIndexes(first_element, second_element)
{
    var formulas_array = Array.from(gen_formula.sub_formulas_unique)
    return [formulas_array.indexOf(first_element), formulas_array.indexOf(second_element)]
}

function getSetElementIndex(element)
{
    var element_aux = element.slice(1)
    return gen_set.sub_formulas_set.indexOf(element_aux)
}

function getSetElementsIndexes(first_element, second_element)
{
    return [gen_set.sub_formulas_set.indexOf(first_element), gen_set.sub_formulas_set.indexOf(second_element)]
}

export function evaluateTable()
{
    var lines = (2**gen_formula.letters_unique.size)
    valoration = []
    for (var i = 0; i < lines; i++)
    {
        valoration.push([])
        var j = gen_formula.letters_unique.size-1;
        for (var element of gen_formula.sub_formulas_unique)
        {
            if (gen_formula.letters_unique.has(element))
            {
                if (((i >> j) & 0x1) == 0)
                    valoration[i].push(true)
                else
                    valoration[i].push(false)

                j--
            }
            else
            {
                if (element[0] == i18n.connectors_sin[0])
                {
                    var index = getElementIndex(element)
                    if (index >= 0)
                    {
                        valoration[i].push(!valoration[i][index])
                    }
                }
                else if (element[0] == "(")
                {
                    var [connector, first_element, second_element] = parser.findConnector(element)
                    if (connector == 0)
                    {
                        var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                        valoration[i].push(valoration[i][first_index] && valoration[i][second_index])
                    }
                    else if (connector == 1)
                    {
                        var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                        valoration[i].push(valoration[i][first_index] || valoration[i][second_index])
                    }
                    else if (connector == 2)
                    {
                        var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                        valoration[i].push(!valoration[i][first_index] || valoration[i][second_index])
                    }
                    else if (connector == 3)
                    {
                        var [first_index, second_index] = getElementsIndexes(first_element, second_element)
                        valoration[i].push(valoration[i][first_index] == valoration[i][second_index])
                    }
                }
            }
        }
    }
}

export function evaluateSetTable()
{
    var lines = (2**gen_set.letters_unique_set.size)
    valoration = []
    models = []
    for (var i = 0; i < lines; i++)
    {
        var isModel = true
        valoration.push([])
        var j = gen_set.letters_unique_set.size-1;
        for (var element of gen_set.sub_formulas_unique_set)
        {
            if (gen_set.letters_unique_set.has(element))
            {
                if (((i >> j) & 0x1) == 0)
                    valoration[i].push(true)
                else
                    valoration[i].push(false)

                j--
            }
            else
            {
                if (element[0] == i18n.connectors_sin[0])
                {
                    var index = getSetElementIndex(element)
                    if (index >= 0)
                    {
                        valoration[i].push(!valoration[i][index])
                    }
                    if(gen_set.set.indexOf(element) >= 0 && valoration[i][index])
                    {
                        isModel = false
                    }
                }
                else if (element[0] == "(")
                {
                    var [connector, first_element, second_element] = parser.findConnector(element)
                    if (connector == 0)
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        valoration[i].push(valoration[i][first_index] && valoration[i][second_index])
                        if(gen_set.set.indexOf(element) >= 0 && !(valoration[i][first_index] && valoration[i][second_index]))
                        {
                            isModel = false
                        }
                    }
                    else if (connector == 1)
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        valoration[i].push(valoration[i][first_index] || valoration[i][second_index])
                        if(gen_set.set.indexOf(element) >= 0 && !(valoration[i][first_index] || valoration[i][second_index]))
                        {
                            isModel = false
                        }
                    }
                    else if (connector == 2)
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        valoration[i].push(!valoration[i][first_index] || valoration[i][second_index])
                        if(gen_set.set.indexOf(element) >= 0 && !(!valoration[i][first_index] || valoration[i][second_index]))
                        {
                            isModel = false
                        }
                    }
                    else if (connector == 3)
                    {
                        var [first_index, second_index] = getSetElementsIndexes(first_element, second_element)
                        valoration[i].push(valoration[i][first_index] == valoration[i][second_index])
                        if(gen_set.set.indexOf(element) >= 0 && !(valoration[i][first_index] == valoration[i][second_index]))
                        {
                            isModel = false
                        }
                    }
                }
            }
        }
        if(isModel)
        {
            models.push(i+1)
        }
    }
}

export function checkTable()
{
    var lines = (2**gen_formula.letters_unique.size)
    var wrong = false
    for (var i = 0; i < lines; i++)
    {
        for (var j = 0; j < gen_formula.sub_formulas_unique.size; j++)
        {
            var element = document.getElementById("l" + i + "c" + j);
            if (((element.value == i18n.text_truth_capital || element.value == i18n.text_truth_lower) && valoration[i][j]) 
                || ((element.value == i18n.text_false_capital || element.value == i18n.text_false_lower) && !valoration[i][j]))
            {
                element.style.border = "2px solid green"
            }
            else
            {
                wrong = true
                element.style.border = "2px solid red"
            }
        }
    }
    if(!wrong)
    {
        formula_questions.nextQuestion("checkTable")
    }
}

export function checkSetTable()
{
    var lines = (2**gen_set.letters_unique_set.size)
    var wrong = false
    for (var i = 0; i < lines; i++)
    {
        for (var j = 0; j < gen_set.sub_formulas_unique_set.size; j++)
        {
            var element = document.getElementById("l" + i + "c" + j);
            if (((element.value == i18n.text_truth_capital || element.value == i18n.text_truth_lower) && valoration[i][j]) 
                || ((element.value == i18n.text_false_capital || element.value == i18n.text_false_lower) && !valoration[i][j]))
            {
                element.style.border = "2px solid green"
            }
            else
            {
                wrong = true
                element.style.border = "2px solid red"
            }
        }
    }
    if(!wrong)
    {
        set_questions_view.nextQuestion("checkTable")
    }
}

export function checkEquivalencyTable()
{
    var lines = (2**gen_set.letters_unique_set.size)
    var wrong = false
    for (var i = 0; i < lines; i++)
    {
        for (var j = 0; j < gen_set.sub_formulas_unique_set.size; j++)
        {
            var element = document.getElementById("l" + i + "c" + j);
            if (((element.value == i18n.text_truth_capital || element.value == i18n.text_truth_lower) && valoration[i][j]) 
                || ((element.value == i18n.text_false_capital || element.value == i18n.text_false_lower) && !valoration[i][j]))
            {
                element.style.border = "2px solid green"
            }
            else
            {
                wrong = true
                element.style.border = "2px solid red"
            }
        }
    }
    if(!wrong)
    {
        equivalency_questions_view.nextQuestion("checkTable")
    }
}

export function checkInferenceTable()
{
    var lines = (2**gen_set.letters_unique_set.size)
    var wrong = false
    for (var i = 0; i < lines; i++)
    {
        for (var j = 0; j < gen_set.sub_formulas_unique_set.size; j++)
        {
            var element = document.getElementById("l" + i + "c" + j);
            if (((element.value == i18n.text_truth_capital || element.value == i18n.text_truth_lower) && valoration[i][j]) 
                || ((element.value == i18n.text_false_capital || element.value == i18n.text_false_lower) && !valoration[i][j]))
            {
                element.style.border = "2px solid green"
            }
            else
            {
                wrong = true
                element.style.border = "2px solid red"
            }
        }
    }
    if(!wrong)
    {
        inference_questions_view.nextQuestion("checkTable")
    }
}