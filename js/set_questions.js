import * as truth_table from "./truth_table.js"
import * as set_questions_view from "../view/set_questions.js"
import * as implication from "./implication.js"

export function isConsistent()
{
    return truth_table.models.length > 0
}

export function checkConsistent()
{
    var elements = document.getElementsByName("consistent")
    var correct = false
    for (var element of elements)
    {
        if (element.value == "yes" && element.checked && isConsistent())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "no" && element.checked && !isConsistent())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
    if (correct)
    {
        set_questions_view.nextQuestion("checkConsistent")
    }
}

export function isTautologyImplication()
{
    var is_tautology = true
    for (var line of truth_table.models)
    {
        if(!implication.implicated_table[line-1][implication.implicated_table[line-1].length-1])
        {
            is_tautology = false
        }
    }
    return is_tautology
}

export function checkTautologyImplication()
{
    var elements = document.getElementsByName("tautology_implication")
    var correct = false
    for (var element of elements)
    {
        if (element.value == "yes" && element.checked && isTautologyImplication())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "no" && element.checked && !isTautologyImplication())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
    if (correct)
    {
        set_questions_view.nextQuestion("checkTautologyImplication")
    }
}

export function isCompatible()
{
    var is_compatible = true
    var count = 0
    for (var line of truth_table.models)
    {
        if(!implication.implicated_table[line-1][implication.implicated_table[line-1].length-1])
        {
            count+=1
        }
    }
    if(count == truth_table.models.length && truth_table.models.length > 0)
    {
        is_compatible = false
    }
    return is_compatible
}

export function checkCompatible()
{
    var elements = document.getElementsByName("compatible")
    var correct = false
    for (var element of elements)
    {
        if (element.value == "yes" && element.checked && isCompatible())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "no" && element.checked && !isCompatible())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
    if (correct)
    {
        set_questions_view.nextQuestion("checkCompatible")
    }
}

export function checkModelLines()
{
    var correct = 0
    var model_lines = document.getElementById("model_lines").value.split(" ").join("")
    var model_lines_array = model_lines.split(",")
    for (var i = 0; i < model_lines_array.length; i++)
    {
        if(truth_table.models.indexOf(parseInt(model_lines_array)) >= 0)
            correct+=1
    }
    if(correct == model_lines_array.length)
    {
        document.getElementById("model_lines").style.border = "2px solid green"
        set_questions_view.nextQuestion("checkModelLines")
    }
    else
    {
        document.getElementById("model_lines").style.border = "2px solid red"
    }
}