import * as gen_formula_quantifier from "../js/formula_quantifier_generator.js"
import * as formula_quantifier_questions_view from "../view/formula_quantifier_questions.js"
import * as i18n from "../i18n/formula.js"
import * as variable_occurrences from "./variable_occurrences.js"

export var number_occurrences = 0
export var is_variable = false

export function hasAtomic()
{
    console.log(gen_formula_quantifier.sub_formulas_atomics)
    return (gen_formula_quantifier.sub_formulas_atomics).size > 0
}

export function hasGeneralsForm()
{
    console.log(gen_formula_quantifier.sub_formulas_generals)
    return gen_formula_quantifier.sub_formulas_generals.size > 0
}

export function hasGeneralsSentence()
{
    console.log(gen_formula_quantifier.sub_formulas_generals_sentence)
    return gen_formula_quantifier.sub_formulas_generals_sentence.size > 0
}

export function hasGeneralsNotSentence()
{
    console.log(gen_formula_quantifier.sub_formulas_not_generals_sentence)
    return gen_formula_quantifier.sub_formulas_not_generals_sentence.size > 0
}

export function hasMolecular()
{
    console.log(gen_formula_quantifier.sub_formulas_molecular)
    return gen_formula_quantifier.sub_formulas_molecular.size > 0
}

export function checkOccurrences()
{
    var occurrences = parseInt(document.getElementById("occurrences").value)
    if(occurrences == number_occurrences)
    {
        document.getElementById("occurrences").style.border = "2px solid green"
        formula_quantifier_questions_view.nextQuestion("checkOccurrency")
    }
    else
    {
        document.getElementById("occurrences").style.border = "2px solid red"
    }
}

export function checkOccurrencesType()
{
    var correct = 0
    for (var i = 0; i < number_occurrences; i++)
    {
        var occurrences_types_values = document.getElementsByName("occurrences_type"+i)
        for (var element of occurrences_types_values)
        {
            if(element.value == "connected" && variable_occurrences.isConnected(i) && element.checked)
            {
                element.nextSibling.style.border = "2px solid green"
                correct += 1
            }
            else if(element.value == "free" && variable_occurrences.isFree(i) && element.checked)
            {
                element.nextSibling.style.border = "2px solid green"
                correct += 1
            }
            else if (element.checked)
            {
                element.nextSibling.style.border = "2px solid red"
            }
            else 
            {
                element.nextSibling.style.border = "none"
            }
        }
    }
    if(correct == number_occurrences)
    {
        formula_quantifier_questions_view.nextQuestion("checkOccurrencesType")
    }
}

export function checkFormulaType()
{
    variable_occurrences.setOccurrencesTypes([])
    for (var variable of gen_formula_quantifier.variables_used)
    {
        variable_occurrences.setVariableGenerated(variable)
        variable_occurrences.evaluateOccurrences(gen_formula_quantifier.sub_formulas, false)
    }
    var formula_types = document.getElementsByName("formula_type")
    for (var element of formula_types)
    {
        if(element.value == "open" && variable_occurrences.isOpen() && element.checked)
        {
            element.nextSibling.style.border = "2px solid green"
            formula_quantifier_questions_view.nextQuestion("checkFormulaType")
        }
        else if(element.value == "closed" && variable_occurrences.isClosed() && element.checked)
        {
            element.nextSibling.style.border = "2px solid green"
            formula_quantifier_questions_view.nextQuestion("checkFormulaType")
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
}

export function checkIsSentence()
{
    var formula_types = document.getElementsByName("is_sentence")
    for (var element of formula_types)
    {
        if(element.value == "yes" && variable_occurrences.isClosed() && element.checked)
        {
            element.nextSibling.style.border = "2px solid green"
            formula_quantifier_questions_view.nextQuestion("checkIsSentence")
        }
        else if(element.value == "no" && variable_occurrences.isOpen() && element.checked)
        {
            element.nextSibling.style.border = "2px solid green"
            formula_quantifier_questions_view.nextQuestion("checkIsSentence")
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
}

export function generateRandomOccurrency()
{
    variable_occurrences.setOccurrencesTypes([])
    if(gen_formula_quantifier.variables_used.size > 0)
    {
        variable_occurrences.setVariableGenerated([...gen_formula_quantifier.variables_used][Math.floor(Math.random() * gen_formula_quantifier.variables_used.size)])
        number_occurrences = (gen_formula_quantifier.sub_formulas[gen_formula_quantifier.sub_formulas.length-1].match(new RegExp(variable_occurrences.variable_generated, "g")) || []).length
        variable_occurrences.evaluateOccurrences(gen_formula_quantifier.sub_formulas, false)
        is_variable = true
        return variable_occurrences.variable_generated
    }
    else if(gen_formula_quantifier.constants_used.size > 0)
    {
        var constant_aux = [...gen_formula_quantifier.constants_used][Math.floor(Math.random() * gen_formula_quantifier.constants_used.size)]
        number_occurrences = (gen_formula_quantifier.sub_formulas[gen_formula_quantifier.sub_formulas.length-1].match(new RegExp(constant_aux, "g")) || []).length
        is_variable = false
        return constant_aux
    }
    else if(gen_formula_quantifier.letters_unique.size > 0)
    {
        var letter_aux = [...gen_formula_quantifier.letters_unique][Math.floor(Math.random() * gen_formula_quantifier.letters_unique.size)]
        number_occurrences = (gen_formula_quantifier.sub_formulas[gen_formula_quantifier.sub_formulas.length-1].match(new RegExp(letter_aux, "g")) || []).length
        is_variable = false
        return letter_aux
    }
}

export function checkImediates()
{
    var correct = 0
    var imediate = document.getElementById("subform_imed").value.split(" ").join("")
    var imediate_array = imediate.split(",")
    console.log(gen_formula_quantifier.sub_formulas)
    if(imediate_array.length == gen_formula_quantifier.sub_formulas.length-1)
    {
        for (var i = 0; i < imediate_array.length; i++)
        {
            if(gen_formula_quantifier.sub_formulas[i].indexOf(imediate_array[i]) >= 0)
            {
                correct += 1
            }
        }
    }
    if(correct == imediate_array.length)
    {
        document.getElementById("subform_imed").style.border = "2px solid green"
        formula_quantifier_questions_view.nextQuestion("checkImediate")
    }
    else
    {
        document.getElementById("subform_imed").style.border = "2px solid red"
    }
}

export function checkAtomics()
{
    var correct = 0
    var atomics = document.getElementById("subform_atom").value.split(" ").join("")
    var atomics_array = atomics.split(",")
    if(atomics_array.length == gen_formula_quantifier.sub_formulas_atomics.size)
    {
        for (var i = 0; i < atomics_array.length; i++)
        {
            if(gen_formula_quantifier.sub_formulas_atomics.has(atomics_array[i]))
            {
                correct += 1
            }
        }
    }
    if(correct == atomics_array.length)
    {
        document.getElementById("subform_atom").style.border = "2px solid green"
        formula_quantifier_questions_view.nextQuestion("checkAtomics")
    }
    else
    {
        document.getElementById("subform_atom").style.border = "2px solid red"
    }
}

export function checkGeneralForm()
{
    var correct = 0
    var generals = document.getElementById("subform_general").value.split(" ").join("")
    var generals_array = generals.split(",")
    if(generals_array.length == gen_formula_quantifier.sub_formulas_generals.size)
    {
        for (var i = 0; i < generals_array.length; i++)
        {
            if(gen_formula_quantifier.sub_formulas_generals.has(generals_array[i]))
            {
                correct += 1
            }
        }
    }
    if(correct == generals_array.length)
    {
        document.getElementById("subform_general").style.border = "2px solid green"
        formula_quantifier_questions_view.nextQuestion("checkGeneralForm")
    }
    else
    {
        document.getElementById("subform_general").style.border = "2px solid red"
    }
}

export function checkGeneralSentence()
{
    var correct = 0
    var generals_sentence = document.getElementById("subform_general_sentence").value.split(" ").join("")
    var generals_sentence_array = generals_sentence.split(",")
    if(generals_sentence_array.length == gen_formula_quantifier.sub_formulas_generals_sentence.size)
    {
        for (var i = 0; i < generals_sentence_array.length; i++)
        {
            if(gen_formula_quantifier.sub_formulas_generals_sentence.has(generals_sentence_array[i]))
            {
                correct += 1
            }
        }
    }
    if(correct == generals_sentence_array.length)
    {
        document.getElementById("subform_general_sentence").style.border = "2px solid green"
        formula_quantifier_questions_view.nextQuestion("checkGeneralSentence")
    }
    else
    {
        document.getElementById("subform_general_sentence").style.border = "2px solid red"
    }
}

export function checkNotGeneralSentence()
{
    var correct = 0
    var generals_not_sentence = document.getElementById("subform_not_general_sentence").value.split(" ").join("")
    var generals_not_sentence_array = generals_not_sentence.split(",")
    if(generals_not_sentence_array.length == gen_formula_quantifier.sub_formulas_not_generals_sentence.size)
    {
        for (var i = 0; i < generals_not_sentence_array.length; i++)
        {
            if(gen_formula_quantifier.sub_formulas_not_generals_sentence.has(generals_not_sentence_array[i]))
            {
                correct += 1
            }
        }
    }
    if(correct == generals_not_sentence_array.length)
    {
        document.getElementById("subform_not_general_sentence").style.border = "2px solid green"
        formula_quantifier_questions_view.nextQuestion("checkNotGeneralSentence")
    }
    else
    {
        document.getElementById("subform_not_general_sentence").style.border = "2px solid red"
    }
}

export function checkMolecular()
{
    var correct = 0
    var moleculars = document.getElementById("subform_molecular").value.split(" ").join("")
    var moleculars_array = moleculars.split(",")
    if(moleculars_array.length == gen_formula_quantifier.sub_formulas_molecular.size)
    {
        for (var i = 0; i < moleculars_array.length; i++)
        {
            if(gen_formula_quantifier.sub_formulas_molecular.has(moleculars_array[i]))
            {
                correct += 1
            }
        }
    }
    if(correct == moleculars_array.length)
    {
        document.getElementById("subform_molecular").style.border = "2px solid green"
        formula_quantifier_questions_view.nextQuestion("checkMolecular")
    }
    else
    {
        document.getElementById("subform_molecular").style.border = "2px solid red"
    }
}