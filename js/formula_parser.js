import * as i18n from "../i18n/formula.js"

export function findConnector(element)
{
    var open_bracket = 0
    var connector_index = -1;
    var first_element = ""
    var second_element = ""
    var get_element = false
    for (var i = 0; i < element.length; i++)
    {
        if (element[i] == "(")
        {
            open_bracket += 1
            if(open_bracket == 1)
            {
                get_element = true
            }
        }
        else if (element[i] == ")")
        {
            open_bracket -= 1
        }

        if (get_element && connector_index == -1)
        {
            first_element += element[i]
        }
        else if (get_element && connector_index != -1)
        {
            second_element += element[i]
        }

        var index = i18n.connectors_bin.indexOf(element[i])
        if (index >= 0 && open_bracket == 1)
        {
            connector_index = index
        }
    }
    first_element = first_element.slice(1, first_element.length-1)
    second_element = second_element.slice(0, second_element.length-1)
    return [connector_index, first_element, second_element]
}