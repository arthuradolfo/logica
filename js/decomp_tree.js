import * as decomp_tree_view from "../view/decomp_tree.js"

export function checkAnswer(level, formulas)
{
    var wrong = false
    if(!Array.isArray(formulas))
        return
    for (var i = 0; i < formulas.length; i++)
    {
        var id = "l" + level + "n" + (i+1)
        var element = document.getElementById(id)
        if (element.value.split(" ").join("") == formulas[i][formulas[i].length-1])
            element.style.border = "2px solid green"
        else
        {   
            element.style.border = "2px solid red"
            wrong = true
        }
    }
    if(!wrong)
    {
        decomp_tree_view.updateDecompTree(level, formulas)
    }
}