import * as i18n from "../i18n/formula.js";
import * as variable_occurrences from "./variable_occurrences.js";

var sprintf = (str, grade) => str = str.replaceAll("&", grade)
export var sub_formulas = []
export var sub_formulas_unique = new Set
export var letters_unique = new Set
export var variables_used = new Set
export var constants_used = new Set
export var sub_formulas_atomics = new Set
export var sub_formulas_generals = new Set
export var sub_formulas_generals_sentence = new Set
export var sub_formulas_not_generals_sentence = new Set
export var sub_formulas_molecular = new Set

function isSentence(formula)
{
    variable_occurrences.setOccurrencesTypes([])
    for(var variable of variables_used)
    {
        variable_occurrences.setVariableGenerated(variable)
        variable_occurrences.evaluateOccurrences(formula, false)
    }
    return variable_occurrences.isClosed()
}

function randomLetter(letters)
{
    var letter = i18n.letters[Math.floor(Math.random() * 5)]
    if(letters == undefined || letters.indexOf(letter) >= 0)
        return letter
    else
        return randomLetter(letters)
}

function randomVariable()
{
    return i18n.variables[Math.floor(Math.random() * 5)]
}

function randomQuantifier()
{
    return i18n.quantifiers[Math.floor(Math.random() * 2)]
}

export function randomConnector()
{
    var connType = Math.floor(Math.random() * 2)
    if (connType == 0)
    {
        return [i18n.connectors_bin[Math.floor(Math.random() * 4)], true]
    }
    else 
    {
        connType = Math.floor(Math.random() * 2)
        if(connType == 0)
        {
            return [i18n.connectors_sin[0], false]
        }
        else
        {
            var variable_aux = randomVariable()
            variables_used.add(variable_aux)
            return [randomQuantifier()+variable_aux, false]
        }
    }
}

function randomAtomic(letters)
{
    var atomic_type = Math.floor(Math.random() * 2)
    if (atomic_type == 0)
    {
        return randomLetter(letters)
    }
    else 
    {
        var grade = Math.floor(Math.random() * 3)
        var grade_string = i18n.grades[grade]
        var variables = ""
        for (var i = 0; i <= grade; i++)
        {
            var is_variable = Math.floor(Math.random() * 2)
            if(is_variable == 1)
            {
                var variable_aux = randomVariable()
                variables += variable_aux
                variables_used.add(variable_aux)
            }
            else
            {
                var variable_aux = i18n.constants[Math.floor(Math.random() * 20)]
                variables += variable_aux
                constants_used.add(variable_aux)
            }
        }
        return sprintf(i18n.predicates[Math.floor(Math.random() * 5)], grade_string)+variables
    }
}

function genLeft(complexity, difficulty, letters)
{
    var sub_sub_formulas = []
    var sub_sub_sub_formulas = []

    var formula = ""
    var formula_aux = ""
    var connector = ""
    var binary = false

    if(complexity == 0) // Last nodes of formula tree
    {
        var type = Math.floor(Math.random() * 2)
        if (type == 0 || difficulty == 0) // Can be a simple letter
        {
            var letter = randomAtomic(letters)
            sub_sub_formulas.push(letter)
            sub_formulas_atomics.add(letter)
            if(isSentence(sub_sub_formulas)) sub_formulas_not_generals_sentence.add(letter)
            return [letter, sub_sub_formulas]
        }
        else if (type == 1) // Or a formula with complexity 1
        {
            var [connector,binary] = randomConnector()
            if(binary)
            {
                var letter = randomAtomic(letters)
                sub_sub_formulas.push([letter])
                if(isSentence([letter])) sub_formulas_not_generals_sentence.add(letter)
                sub_formulas_atomics.add(letter)
                formula = "(" + letter
                formula += connector
                letter = randomAtomic(letters)
                sub_sub_formulas.push([letter])
                if(isSentence([letter])) sub_formulas_not_generals_sentence.add(letter)
                sub_formulas_atomics.add(letter)
                formula += letter
                formula += ")"
                sub_sub_formulas.push(formula)
                sub_formulas_molecular.add(formula)
                if(isSentence(sub_sub_formulas)) sub_formulas_not_generals_sentence.add(formula)
            }
            else
            {
                var letter = randomAtomic(letters)
                sub_formulas_atomics.add(letter)
                if(isSentence([letter])) sub_formulas_not_generals_sentence.add(letter)
                sub_sub_formulas.push([letter])
                formula = connector+letter
                sub_sub_formulas.push(formula)
                if(i18n.quantifiers.indexOf(connector[0]) >= 0) 
                {
                    sub_formulas_generals.add(formula)
                    if(isSentence(sub_sub_formulas)) sub_formulas_generals_sentence.add(formula)
                }
                else
                {
                    sub_formulas_molecular.add(formula)
                    if(isSentence(sub_sub_formulas)) sub_formulas_not_generals_sentence.add(formula)
                }
            }
            return [formula, sub_sub_formulas]
        } 
    }
    
    // Generate left side formulas recursevely
    var[formula, sub_sub_sub_formulas] = genLeft(complexity-1, difficulty, letters)
    sub_sub_formulas.push(sub_sub_sub_formulas) // store sub_formulas generated in left side

    // Generate right side formulas recusevely ONLY if connector is binary
    var [connector,binary] = randomConnector()
    if(binary)
    {
        formula = "(" + formula
        formula += connector
        var [formula_aux, sub_sub_sub_formulas] = genLeft(complexity-1, difficulty, letters) //right side formulas
        sub_sub_formulas.push(sub_sub_sub_formulas) // store sub_formulas generated in right side

        formula += formula_aux
        formula += ")"
        sub_formulas_molecular.add(formula)
        sub_sub_formulas.push(formula) // store bigger formula
        if(isSentence(sub_sub_formulas)) sub_formulas_not_generals_sentence.add(formula)
    }
    else
    {
        formula = connector+formula
        sub_sub_formulas.push(formula) // store bigger formula
        if(i18n.quantifiers.indexOf(connector[0]) >= 0) 
        {
            sub_formulas_generals.add(formula)
            if(isSentence(sub_sub_formulas)) sub_formulas_generals_sentence.add(formula)
        }
        else
        {
            sub_formulas_molecular.add(formula)
            if(isSentence(sub_sub_formulas)) sub_formulas_not_generals_sentence.add(formula)
        }
    }
    return [formula, sub_sub_formulas]
}

function genFormula(complexity, difficulty, letters)
{
    sub_formulas = []
    sub_formulas_unique = new Set
    letters_unique = new Set
    constants_used = new Set
    variables_used = new Set
    sub_formulas_atomics = new Set
    sub_formulas_generals = new Set
    sub_formulas_generals_sentence = new Set
    sub_formulas_not_generals_sentence = new Set
    sub_formulas_molecular = new Set
    var sub_sub_formulas = []
    if (complexity == 1) // generate formula recursevely
    {
        var [formula, sub_sub_formulas] = genLeft(complexity, difficulty, letters)
        sub_formulas.push(sub_sub_formulas) // store sub_formulas generated
        return formula
    }
    else
    {
        var formula = ""
        var formula_aux = ""
        var connector = ""
        var binary = false
        var [formula,sub_sub_formulas]  = genLeft(complexity-1, difficulty, letters)
        sub_formulas.push(sub_sub_formulas) // store sub_formulas generated
        
        var [connector,binary] = randomConnector()
        if(binary)
        {
            formula = "(" + formula
            formula += connector
            var [formula_aux, sub_sub_formulas] = genLeft(complexity-1, difficulty, letters)
            sub_formulas.push(sub_sub_formulas) // store sub_formulas generated

            formula += formula_aux
            formula += ")"
            sub_formulas_molecular.add(formula)
            sub_formulas.push(formula) // store sub_formulas generated
            if(isSentence(sub_formulas)) sub_formulas_not_generals_sentence.add(formula)
        }
        else
        {
            formula = connector+formula
            sub_formulas.push(formula) // store sub_formulas generated
            if(i18n.quantifiers.indexOf(connector[0]) >= 0) 
            {
                sub_formulas_generals.add(formula)
                if(isSentence(sub_formulas)) sub_formulas_generals_sentence.add(formula)
            }
            else
            {
                sub_formulas_molecular.add(formula)
                if(isSentence(sub_formulas)) sub_formulas_not_generals_sentence.add(formula)
            }
        }
        return formula
    }
}

export function generateFormula(complexity, difficulty, letters)
{
    var formula_gerada = genFormula(complexity, difficulty, letters)

    var sub_formulas_flat = sub_formulas.flat(100)
    var sub_formulas_flat_sort = sub_formulas_flat.sort((a,b) => a.length - b.length);
    for (var i = 0; i < sub_formulas_flat_sort.length; i++)
    {
        sub_formulas_unique.add(sub_formulas_flat_sort[i])
        if(sub_formulas_flat_sort[i].length == 1)
            letters_unique.add(sub_formulas_flat_sort[i])
    }

    return formula_gerada
}

export function removeFormulasFromArray(toRemove)
{
    for (var elem of toRemove) {
        sub_formulas_unique.delete(elem);
    }
}