import * as parser from "../js/formula_parser.js"
import * as gen_formula from "../js/formula_generator.js"
import * as truth_table from "../js/truth_table.js"
import * as formula_questions_view from "../view/formula_questions.js"
import * as i18n from "../i18n/formula.js"

function isContradiction()
{
    var contradiction = true
    for (var i  = 0; i < truth_table.valoration.length; i++)
    {
        if (truth_table.valoration[i][truth_table.valoration[i].length-1])
        {
            contradiction = false
        }
    }
    return contradiction
}

function isTautology()
{
    var tautology = true
    for (var i  = 0; i < truth_table.valoration.length; i++)
    {
        if (!truth_table.valoration[i][truth_table.valoration[i].length-1])
        {
            tautology = false
        }
    }
    return tautology
}

export function checkTautology()
{
    var elements = document.getElementsByName("tautologia")
    var correct = false
    for (var element of elements)
    {
        if (element.value == "tautologia" && element.checked && isTautology())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "contradicao" && element.checked && isContradiction())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "contingencia" && element.checked && (!isContradiction() && !isTautology()))
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
    if (correct)
    {
        formula_questions_view.nextQuestion("checkTautology")
    }
}

export function checkSubformulas()
{
    var certas = 0
    var subformulas = document.getElementById("subform").value.split(" ").join("")
    var subformulas_array = subformulas.split(",")
    var formulas_array = Array.from(gen_formula.sub_formulas_unique)
    if(subformulas_array.length == formulas_array.length)
    {
        for (var i = 0; i < subformulas_array.length; i++)
        {
            if(formulas_array.indexOf(subformulas_array[i]) >= 0)
            {
                certas += 1
            }
        }
    }
    if(certas == subformulas_array.length)
    {
        document.getElementById("subform").style.border = "2px solid green"
        formula_questions_view.nextQuestion("checkSubformulas")
    }
    else
    {
        document.getElementById("subform").style.border = "2px solid red"
    }
}

export function checkImediates()
{
    var correct = 0
    var imediate = document.getElementById("subform_imed").value.split(" ").join("")
    var imediate_array = imediate.split(",")
    if(imediate_array.length == gen_formula.sub_formulas.length-1)
    {
        for (var i = 0; i < imediate_array.length; i++)
        {
            if(gen_formula.sub_formulas[i].indexOf(imediate_array[i]) >= 0)
            {
                correct += 1
            }
        }
    }
    if(correct == imediate_array.length)
    {
        document.getElementById("subform_imed").style.border = "2px solid green"
        formula_questions_view.nextQuestion("checkImediate")
    }
    else
    {
        document.getElementById("subform_imed").style.border = "2px solid red"
    }
}

function isAtomic()
{
    return gen_formula.sub_formulas[gen_formula.sub_formulas.length-1].size == 1
}

function isNegation()
{
    return gen_formula.sub_formulas[gen_formula.sub_formulas.length-1][0] == i18n.connectors_sin[0]
}

function isConjunction()
{
    var [connector, first_element, second_element] = parser.findConnector(gen_formula.sub_formulas[gen_formula.sub_formulas.length-1])
    return connector == 0
}

function isDisjunction()
{
    var [connector, first_element, second_element] = parser.findConnector(gen_formula.sub_formulas[gen_formula.sub_formulas.length-1])
    return connector == 1
}

function isImplication()
{
    var [connector, first_element, second_element] = parser.findConnector(gen_formula.sub_formulas[gen_formula.sub_formulas.length-1])
    return connector == 2
}

function isEquivalent()
{
    var [connector, first_element, second_element] = parser.findConnector(gen_formula.sub_formulas[gen_formula.sub_formulas.length-1])
    return connector == 3
}

export function checkFormat()
{
    var elements = document.getElementsByName("format")
    var correct = false
    for (var element of elements)
    {
        if (element.value == "atomic" && element.checked && isAtomic())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "negation" && element.checked && isNegation())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "conjunction" && element.checked && isConjunction())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "disjunction" && element.checked && isDisjunction())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "implication" && element.checked && isImplication())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "equivalency" && element.checked && isEquivalent())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
    if (correct)
    {
        formula_questions_view.nextQuestion("checkFormat")
    }
}

export function hasModel()
{
    var has_model = false
    for (var lines of truth_table.valoration)
    {
        if(lines[lines.length-1])
        {
            has_model = true
        }
    }
    return has_model
}

export function checkModel()
{
    var elements = document.getElementsByName("model")
    var correct = false
    for (var element of elements)
    {
        if (element.value == "yes" && element.checked && hasModel())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "no" && element.checked && !hasModel())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
    if (correct)
    {
        formula_questions_view.nextQuestion("checkModel")
    }
}

export function checkModelLines()
{
    var correct = 0
    var model_lines = document.getElementById("model_lines").value.split(" ").join("")
    var model_lines_array = model_lines.split(",")
    for (var i = 0; i < model_lines_array.length; i++)
    {
        if(truth_table.valoration[parseInt(model_lines_array[i])-1][truth_table.valoration[parseInt(model_lines_array[i])-1].length-1])
        {
            correct += 1
        }
    }
    if(correct == model_lines_array.length)
    {
        document.getElementById("model_lines").style.border = "2px solid green"
        formula_questions_view.nextQuestion("checkModelLines")
    }
    else
    {
        document.getElementById("model_lines").style.border = "2px solid red"
    }
}