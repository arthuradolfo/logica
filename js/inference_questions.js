import * as gen_inference from "../js/inference_generator.js"
import * as inference_questions_view from "../view/inference_questions.js"
import * as truth_table from "./truth_table.js"

export function isInference()
{
    var is_tautology = true
    for (var i = 0; i < truth_table.valoration.length; i++)
    {
        if(truth_table.valoration[i][truth_table.valoration[i].length-3] && truth_table.valoration[i][truth_table.valoration[i].length-2] && !truth_table.valoration[i][truth_table.valoration[i].length-1])
        {
            is_tautology = false
        }
    }
    return is_tautology
}

export function checkInference()
{
    var elements = document.getElementsByName("inference")
    var correct = false
    for (var element of elements)
    {
        if (parseInt(element.value) == gen_inference.inference_generated && element.checked)
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
    if (correct)
    {
        inference_questions_view.nextQuestion("checkInference")
    }
}

export function checkIsInference()
{
    var elements = document.getElementsByName("isInference")
    var correct = false
    for (var element of elements)
    {
        if (element.value == "yes" && element.checked && isInference())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "no" && element.checked && !isInference())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
    if (correct)
    {
        inference_questions_view.nextQuestion("checkIsInference")
    }
}
