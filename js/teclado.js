import * as i18n from "../i18n/arvore_decomp.js"

export var focus

export function addValue(element)
{
    console.log(focus)
    if(focus)
    {
        document.getElementById(focus).value += element.target.innerText
        document.getElementById(focus).focus()
    }
}

export function removeValue(element)
{
    if(focus)
    {
        document.getElementById(focus).value = document.getElementById(focus).value.slice(0,document.getElementById(focus).value.length-1)
        document.getElementById(focus).focus()
    }
}

export function getLatestFocus()
{
    var elements = document.getElementsByTagName("input")
    for(var element of elements)
    {
        if(element.type == "text")
        {
            element.onfocus = function () {
                focus = this.id; 
                this.style.border = "2px groove blue"
            }
            element.onblur = function () {
                this.style.border = "1px groove gray"
            }
        }
    }
}