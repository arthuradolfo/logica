import * as i18n from "../i18n/formula.js"

export var occurrences_types = []
export var variable_generated

export function isConnected(occurrency)
{
    return occurrences_types[occurrency]
}

export function isFree(occurrency)
{
    return !isConnected(occurrency)
}

export function isOpen()
{
    for (var i = 0; i < occurrences_types.length; i++)
    {
        if(!occurrences_types[i])
        {
            return true
        }
    }
    return false
}

export function isClosed()
{
    return !isOpen()
}

export function setVariableGenerated(variable)
{
    variable_generated = variable
}

export function setOccurrencesTypes(occurences)
{
    occurrences_types = occurences
}

function isQuantifier(element)
{
    return (element[0] == i18n.quantifiers[0] || element[0] == i18n.quantifiers[1])
}

function hasVariable(element)
{
    var counter = 0
    for (var i = 0; i < element.length; i++)
    {
        if((element[i] == i18n.quantifiers[0] || element[i] == i18n.quantifiers[1]))
        {
            if(element[i+1] == variable_generated)
            {
                counter+=1
                break
            }
            else
            {
                break
            }
        }
        else if(element[i] == i18n.parenthesis[0] || element[i] == i18n.connectors_sin[0])
        {
            break
        }
        else if(element[i] == variable_generated)
        {
            counter+=1
        }
    }
    return counter
}

export function evaluateOccurrences(formulas, is_connected)
{
    if(isQuantifier(formulas[formulas.length-1]) && hasVariable(formulas[formulas.length-1]))
    {
        for (var i = 0; i < hasVariable(formulas[formulas.length-1]); i++)
        {
            occurrences_types.push(1)
        }
        is_connected = true
    }
    else if(hasVariable(formulas[formulas.length-1]))
    {
        for (var i = 0; i < hasVariable(formulas[formulas.length-1]); i++)
        {
            occurrences_types.push(is_connected ? 1 : 0)
        }
    }
    for (var element of formulas)
    {
        if(typeof element == "string")
        {
            continue
        }
        else
        {
            evaluateOccurrences(element, is_connected)
        }
    }
}