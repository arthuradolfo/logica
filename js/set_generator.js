import * as gen_formula from "./formula_generator.js"

export var set = []
export var sub_formulas_set = []
export var sub_formulas_unique_set = new Set
export var letters_unique_set = new Set

export function generateSet(size, complexity_array)
{
    set = []
    sub_formulas_set = []
    sub_formulas_unique_set = new Set
    letters_unique_set = new Set
    for (var i = 0; i < size; i++)
    {
        set.push(gen_formula.generateFormula(complexity_array[i][0], complexity_array[i][1]))
        for (var element of gen_formula.sub_formulas_unique)
        {
            if(set.indexOf(element) < 0)
            {
                sub_formulas_unique_set.add(element)
                if(element.length == 1)
                    letters_unique_set.add(element)
            }
        }
    }
    sub_formulas_set.push(...sub_formulas_unique_set)

    sub_formulas_unique_set = new Set
    var sub_formulas_flat_sort = sub_formulas_set.sort((a,b) => a.length - b.length);
    for (var i = 0; i < sub_formulas_flat_sort.length; i++)
    {
        sub_formulas_unique_set.add(sub_formulas_flat_sort[i])
    }
    
    for (var element of set)
    {
        sub_formulas_set.push(element)
        sub_formulas_unique_set.add(element)
    }

    return "{" + Array.from(set).join(',') + "}"
}