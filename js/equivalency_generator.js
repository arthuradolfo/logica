import * as i18n from "../i18n/formula.js";
import * as gen_formula from "./formula_generator.js";
import * as gen_set from "./set_generator.js";
import * as truth_table from "./truth_table.js"

export var sprintf = (str, phi, psi, theta, conn, conn_inv) => str = str.replaceAll("$", phi).replaceAll("%", psi).replaceAll("#", theta).replaceAll("_", conn).replaceAll("!", conn_inv)
var first_valoration = []
var second_valoration = []
var third_valoration = []
export var equivalencies = [
    i18n.parenthesis[0] + i18n.connectors_sin[0] + i18n.connectors_sin[0] + "$" + i18n.connectors_bin[3] + "$" + i18n.parenthesis[1], // Dupla negação
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + "%" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] + i18n.connectors_sin[0] + "$" + i18n.connectors_bin[1] + "%" + i18n.parenthesis[1] + i18n.parenthesis[1], // Definição de → por ¬ e 'ou'
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[0] + "$" + i18n.parenthesis[1] + i18n.connectors_bin[3] + "$" + i18n.parenthesis[1], // Contração da conjunção
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + "_" + "%" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] + i18n.connectors_sin[0] + "%" + "_" + "$" + i18n.parenthesis[1] + i18n.parenthesis[1], // Comutação
    i18n.parenthesis[0] + i18n.connectors_sin[0] + i18n.parenthesis[0] + "$" + "_" + "%" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] + i18n.connectors_sin[0] + "$" + "!"+ i18n.connectors_sin[0] + "%" + i18n.parenthesis[1] + i18n.parenthesis[1], // De Morgan
    i18n.parenthesis[0] + i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + "_" + "%" + i18n.parenthesis[1] + "_" + "#" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] +  "$" + "_" + i18n.parenthesis[0] + "%" + "_" + "#" + i18n.parenthesis[1] + i18n.parenthesis[1], // Associação
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + "%" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] + i18n.connectors_sin[0] + "%" + i18n.connectors_bin[2] + i18n.connectors_sin[0] + "$" + i18n.parenthesis[1] + i18n.parenthesis[1], // Contraposição
    i18n.parenthesis[0] + i18n.connectors_sin[0] + i18n.parenthesis[0] + i18n.connectors_sin[0] + "$" + "_" + i18n.connectors_sin[0] + "%" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] + "$" + "!" + "%" + i18n.parenthesis[1] + i18n.parenthesis[1], // Equivalência de negação e "ou"
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + "_" + i18n.parenthesis[0] + "%" + "!" + "#" + i18n.parenthesis[1] + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + "_" + "%" + i18n.parenthesis[1] + "!" + i18n.parenthesis[0] + "$" + "_" + "#" + i18n.parenthesis[1] + i18n.parenthesis[1] + i18n.parenthesis[1], // Distributiva
    i18n.parenthesis[0] + i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[0] + "%" + i18n.parenthesis[1] + i18n.connectors_bin[2] + "#" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + i18n.parenthesis[0] + "%" + i18n.connectors_bin[2] + "#" + i18n.parenthesis[1] + i18n.parenthesis[1] + i18n.parenthesis[1], // Exportação/Importação
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + "%" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.connectors_sin[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[0] + i18n.connectors_sin[0] + "%" + i18n.parenthesis[1] + i18n.parenthesis[1], // Definição de → por ¬ e 'e'
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[0] + "%" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.connectors_sin[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + i18n.connectors_sin[0] + "%" + i18n.parenthesis[1] + i18n.parenthesis[1], // Definição de 'e' por ¬ e →
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[1] + "%" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] + i18n.connectors_sin[0] + "$" + i18n.connectors_bin[2] + "%" + i18n.parenthesis[1] + i18n.parenthesis[1], // Definição de 'ou' por ¬ e →
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + "_" + i18n.parenthesis[0] + "$" + "_" + "%" + i18n.parenthesis[1] + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] + "$" + "_" + "%" + i18n.parenthesis[1] + i18n.parenthesis[1], // Absorção
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + "_" + i18n.parenthesis[0] + "$" + "!" + "%" + i18n.parenthesis[1] + i18n.parenthesis[1] + i18n.connectors_bin[3] + "$" + i18n.parenthesis[1],
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[3] + "%" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + "%" + i18n.parenthesis[1] + i18n.connectors_bin[0] + i18n.parenthesis[0] + "%" + i18n.connectors_bin[2] + "$" + i18n.parenthesis[1] + i18n.parenthesis[1] + i18n.parenthesis[1],
    i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + i18n.connectors_bin[3] + "%" + i18n.parenthesis[1] + i18n.connectors_bin[3] + i18n.parenthesis[0] + i18n.parenthesis[0] + "$" + "_" + "%" + i18n.parenthesis[1] + "!" + i18n.parenthesis[0] + i18n.connectors_sin[0] + "%" + "_" + i18n.connectors_sin[0] + "$" + i18n.parenthesis[1] + i18n.parenthesis[1] + i18n.parenthesis[1]
]

export var equivalencies_connectors = [
    null,
    null,
    null,
    [i18n.connectors_bin[0],i18n.connectors_bin[1],i18n.connectors_bin[3]],
    [i18n.connectors_bin[0],i18n.connectors_bin[1]],
    [i18n.connectors_bin[0],i18n.connectors_bin[1],i18n.connectors_bin[3]],
    [i18n.connectors_bin[0],i18n.connectors_bin[1],i18n.connectors_bin[3]],
    [i18n.connectors_bin[0],i18n.connectors_bin[1]],
    [i18n.connectors_bin[0],i18n.connectors_bin[1]],
    null,
    null,
    null,
    null,
    [i18n.connectors_bin[0],i18n.connectors_bin[1]],
    [i18n.connectors_bin[0],i18n.connectors_bin[1]],
    null,
    [i18n.connectors_bin[0],i18n.connectors_bin[1]]
]
export var equivalency_generated = 0
export var connector
export var inverse_connector

function randomEquivalency()
{
    equivalency_generated = Math.floor(Math.random() * equivalencies.length)
    return equivalencies[equivalency_generated]
}

export function randomEquivalencyConnector(equivalency)
{
    if(equivalency)
    {
        return equivalencies_connectors[equivalency] ? equivalencies_connectors[equivalency][Math.floor(Math.random() * equivalencies_connectors[equivalency].length)] : ""
    }
    else
    {
        return equivalencies_connectors[equivalency_generated] ? equivalencies_connectors[equivalency_generated][Math.floor(Math.random() * equivalencies_connectors[equivalency_generated].length)] : ""
    }
}

export function generateEquivalency(complexity, difficulty)
{
    first_valoration = []
    second_valoration = []
    third_valoration = []
    var first_formula = gen_formula.generateFormula(complexity, difficulty)
    truth_table.evaluateTable()
    for (var i = 0; i < truth_table.valoration.length; i++)
    {
        first_valoration.push([...truth_table.valoration[i]])
    }
    var second_formula = gen_formula.generateFormula(complexity, difficulty)
    truth_table.evaluateTable()
    for (var i = 0; i < truth_table.valoration.length; i++)
    {
        second_valoration.push([...truth_table.valoration[i]])
    }
    var third_formula = gen_formula.generateFormula(complexity, difficulty)
    truth_table.evaluateTable()
    for (var i = 0; i < truth_table.valoration.length; i++)
    {
        third_valoration.push([...truth_table.valoration[i]])
    }

    var equivalency_aux = randomEquivalency()
    connector = randomEquivalencyConnector()
    inverse_connector = (connector == i18n.connectors_bin[0]) ? i18n.connectors_bin[1] : i18n.connectors_bin[0]

    return sprintf(equivalency_aux, first_formula, second_formula, third_formula, connector, inverse_connector)
}

export function generatePossibleEquivalency(complexity, difficulty)
{
    gen_set.generateSet(2,[[1,0],[1,1]])

    return "(" + gen_set.set[0] + i18n.connectors_bin[3] + gen_set.set[1] + ")"
}