import * as gen_equivalency from "../js/equivalency_generator.js"
import * as equivalency_questions_view from "../view/equivalency_questions.js"
import * as truth_table from "./truth_table.js"

export function isEquivalency()
{
    var is_equivalency = true
    console.log(truth_table.valoration)
    for (var line of truth_table.valoration)
    {
        console.log(line)
        if(line[line.length-1] != line[line.length-2])
        {
            is_equivalency = false
        }
    }
    return is_equivalency
}

export function checkEquivalency()
{
    var elements = document.getElementsByName("equivalency")
    var correct = false
    for (var element of elements)
    {
        if (parseInt(element.value) == gen_equivalency.equivalency_generated && element.checked)
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
    if (correct)
    {
        equivalency_questions_view.nextQuestion("checkEquivalency")
    }
}

export function checkIsEquivalency()
{
    var elements = document.getElementsByName("isEquivalency")
    var correct = false
    for (var element of elements)
    {
        if (element.value == "yes" && element.checked && isEquivalency())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.value == "no" && element.checked && !isEquivalency())
        {
            element.nextSibling.style.border = "2px solid green"
            correct = true
        }
        else if (element.checked)
        {
            element.nextSibling.style.border = "2px solid red"
        }
        else 
        {
            element.nextSibling.style.border = "none"
        }
    }
    if (correct)
    {
        equivalency_questions_view.nextQuestion("checkIsEquivalency")
    }
}
