import * as i18n from "../i18n/formula.js"

window.onload = function() {
    var formula_link = document.getElementById("formula-link")
    formula_link.addEventListener("click", () => { location.href = "formula.html" })
    var p = document.createElement("p")
    p.innerText = i18n.text_title
    formula_link.append(p)
    var set_link = document.getElementById("set-link")
    set_link.addEventListener("click", () => { location.href = "set.html" })
    p = document.createElement("p")
    p.innerText = i18n.text_set_title
    set_link.append(p)
    var equivalency_link = document.getElementById("equivalency-link")
    equivalency_link.addEventListener("click", () => { location.href = "equivalency.html" })
    p = document.createElement("p")
    p.innerText = i18n.text_equivalency_title
    equivalency_link.append(p)
    var equivalency_link = document.getElementById("quantifier-link")
    equivalency_link.addEventListener("click", () => { location.href = "formula_quantifier.html" })
    p = document.createElement("p")
    p.innerText = i18n.text_quantifier_title
    equivalency_link.append(p)
    var inference_link = document.getElementById("inference-link")
    inference_link.addEventListener("click", () => { location.href = "inference.html" })
    p = document.createElement("p")
    p.innerText = i18n.text_inference_title
    inference_link.append(p)
}