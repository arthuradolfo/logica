import * as i18n from "../i18n/formula.js";

export var sub_formulas = []
export var sub_formulas_unique = new Set
export var letters_unique = new Set

function randomLetter(letters)
{
    var letter = i18n.letters[Math.floor(Math.random() * 5)]
    if(letters == undefined || letters.indexOf(letter) >= 0)
        return letter
    else
        return randomLetter(letters)
}

export function randomConnector()
{
    var connType = Math.floor(Math.random() * 2)
    if (connType == 0)
    {
        return [i18n.connectors_bin[Math.floor(Math.random() * 4)], true]
    }
    else 
    {
        return [i18n.connectors_sin[0], false]
    }
}

function genLeft(complexity, difficulty, letters)
{
    var sub_sub_formulas = []
    var sub_sub_sub_formulas = []

    var formula = ""
    var formula_aux = ""
    var connector = ""
    var binary = false

    if(complexity == 0) // Last nodes of formula tree
    {
        var type = Math.floor(Math.random() * 2)
        if (type == 0 || difficulty == 0) // Can be a simple letter
        {
            var letter = randomLetter(letters)
            sub_sub_formulas.push(letter)
            return [letter, sub_sub_formulas]
        }
        else if (type == 1) // Or a formula with complexity 1
        {
            var [connector,binary] = randomConnector()
            if(binary)
            {
                var letter = randomLetter(letters)
                sub_sub_formulas.push([letter])
                formula = "(" + letter
                formula += connector
                letter = randomLetter(letters)
                sub_sub_formulas.push([letter])
                formula += letter
                formula += ")"
                sub_sub_formulas.push(formula)
            }
            else
            {
                var letter = randomLetter(letters)
                sub_sub_formulas.push([letter])
                formula = connector+letter
                sub_sub_formulas.push(formula)
            }
            return [formula, sub_sub_formulas]
        } 
    }
    
    // Generate left side formulas recursevely
    var[formula, sub_sub_sub_formulas] = genLeft(complexity-1, difficulty, letters)
    sub_sub_formulas.push(sub_sub_sub_formulas) // store sub_formulas generated in left side

    // Generate right side formulas recusevely ONLY if connector is binary
    var [connector,binary] = randomConnector()
    if(binary)
    {
        formula = "(" + formula
        formula += connector
        var [formula_aux, sub_sub_sub_formulas] = genLeft(complexity-1, difficulty, letters) //right side formulas
        sub_sub_formulas.push(sub_sub_sub_formulas) // store sub_formulas generated in right side

        formula += formula_aux
        formula += ")"
        sub_sub_formulas.push(formula) // store bigger formula
    }
    else
    {
        formula = connector+formula
        sub_sub_formulas.push(formula) // store bigger formula
    }
    return [formula, sub_sub_formulas]
}

function genFormula(complexity, difficulty, letters)
{
    sub_formulas = []
    sub_formulas_unique = new Set
    letters_unique = new Set
    var sub_sub_formulas = []
    if (complexity == 1) // generate formula recursevely
    {
        var [formula, sub_sub_formulas] = genLeft(complexity, difficulty, letters)
        sub_formulas.push(sub_sub_formulas) // store sub_formulas generated
        return formula
    }
    else
    {
        var formula = ""
        var formula_aux = ""
        var connector = ""
        var binary = false
        var [formula,sub_sub_formulas]  = genLeft(complexity-1, difficulty, letters)
        sub_formulas.push(sub_sub_formulas) // store sub_formulas generated
        
        var [connector,binary] = randomConnector()
        if(binary)
        {
            formula = "(" + formula
            formula += connector
            var [formula_aux, sub_sub_formulas] = genLeft(complexity-1, difficulty, letters)
            sub_formulas.push(sub_sub_formulas) // store sub_formulas generated

            formula += formula_aux
            formula += ")"
            sub_formulas.push(formula) // store sub_formulas generated
        }
        else
        {
            formula = connector+formula
            sub_formulas.push(formula) // store sub_formulas generated
        }
        return formula
    }
}

export function generateFormula(complexity, difficulty, letters)
{
    var formula_gerada = genFormula(complexity, difficulty, letters)

    var sub_formulas_flat = sub_formulas.flat(100)
    var sub_formulas_flat_sort = sub_formulas_flat.sort((a,b) => a.length - b.length);
    for (var i = 0; i < sub_formulas_flat_sort.length; i++)
    {
        sub_formulas_unique.add(sub_formulas_flat_sort[i])
        if(sub_formulas_flat_sort[i].length == 1)
            letters_unique.add(sub_formulas_flat_sort[i])
    }

    return formula_gerada
}

export function removeFormulasFromArray(toRemove)
{
    for (var elem of toRemove) {
        sub_formulas_unique.delete(elem);
    }
}