import * as i18n from "../i18n/formula.js";
import * as gen_formula from "./formula_generator.js";
import * as gen_set from "./set_generator.js";
import * as truth_table from "./truth_table.js"

export var sprintf = (str, phi, psi, theta, conn, conn_inv) => str = str.replaceAll("$", phi).replaceAll("%", psi).replaceAll("#", theta).replaceAll("_", conn).replaceAll("!", conn_inv)
var first_valoration = []
var second_valoration = []
var third_valoration = []
export var inferencies = [
    i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + "%" + i18n.parenthesis[1] + i18n.break_line + "$" + i18n.break_line + i18n.horizontal_line + "%", // Modus Ponens
    i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + "%" + i18n.parenthesis[1] + i18n.break_line + i18n.connectors_sin[0] + "%" + i18n.break_line + i18n.horizontal_line + i18n.connectors_sin[0] + "$", // Modus Tollens
    "$" + i18n.break_line + i18n.horizontal_line + i18n.parenthesis[0] + "%" + i18n.connectors_bin[2] + "$" + i18n.parenthesis[1], // Pré-Fixação
    i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + "%" + i18n.parenthesis[1] + i18n.break_line + i18n.parenthesis[0] + "%" + i18n.connectors_bin[2] + "#" + i18n.parenthesis[1] + i18n.break_line + i18n.horizontal_line + i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + "#" + i18n.parenthesis[1], // Silogismos Hipotéticos
    i18n.parenthesis[0] + "$" + i18n.connectors_bin[1] + "%" + i18n.parenthesis[1] + i18n.break_line + i18n.connectors_sin[0] + "$" + i18n.break_line + i18n.horizontal_line + "%", // Silogismo Disjuntivo
    "$" + i18n.break_line + i18n.connectors_sin[0] + "$" + i18n.break_line + i18n.horizontal_line + "%", // Duns Scotus
    i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + "#" + i18n.parenthesis[1] + i18n.break_line + i18n.parenthesis[0] + "%" + i18n.connectors_bin[2] + "#" + i18n.parenthesis[1] + i18n.break_line + i18n.parenthesis[0] + "$" + i18n.connectors_bin[1] + "%" + i18n.parenthesis[1] + i18n.break_line + i18n.horizontal_line + "#", // Prova por casos
    i18n.parenthesis[0] + "$" + i18n.connectors_bin[0] + "%" + i18n.parenthesis[1] + i18n.break_line + i18n.horizontal_line + "$", // Eliminação do segundo conjuntivo
    i18n.parenthesis[0] + "$" + i18n.connectors_bin[0] + "%" + i18n.parenthesis[1] + i18n.break_line + i18n.horizontal_line + "%", // Eliminação do primeiro conjuntivo
    "$" + i18n.break_line + i18n.horizontal_line + i18n.parenthesis[0] + "$" + i18n.connectors_bin[1] + "%" + i18n.parenthesis[1], // Introdução do segundo disjuntivo
    "%" + i18n.break_line + i18n.horizontal_line + i18n.parenthesis[0] + "$" + i18n.connectors_bin[1] + "%" + i18n.parenthesis[1], // Introdução do primeiro disjuntivo
    i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + "%" + i18n.parenthesis[1] + i18n.break_line + i18n.parenthesis[0] + "$" + i18n.connectors_bin[2] + i18n.connectors_sin[0] + "%" + i18n.parenthesis[1] + i18n.break_line + i18n.horizontal_line + i18n.connectors_sin[0] + "$", // Lei de Redução ao Absurdo
]

export var inferencies_description = [
    "{" + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[2] + i18n.psi + i18n.parenthesis[1] + "," + i18n.phi + "}" + i18n.inference + i18n.psi, // Modus Ponens
    "{" + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[2] + i18n.psi + i18n.parenthesis[1] + "," + i18n.connectors_sin[0] + i18n.psi + "}" + i18n.inference + i18n.connectors_sin[0] + i18n.phi, // Modus Tollens
    "{" + i18n.phi + "}" + i18n.inference + i18n.parenthesis[0] + i18n.psi + i18n.connectors_bin[2] + i18n.phi + i18n.parenthesis[1], // Pré-Fixação
    "{" + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[2] + i18n.psi + i18n.parenthesis[1] + "," + i18n.parenthesis[0] + i18n.psi + i18n.connectors_bin[2] + i18n.theta + i18n.parenthesis[1] + "}" + i18n.inference + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[2] + i18n.theta + i18n.parenthesis[1], // Silogismos Hipotéticos
    "{" + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[1] + i18n.psi + i18n.parenthesis[1] + "," + i18n.connectors_sin[0] + i18n.phi + "}" + i18n.inference + i18n.psi, // Silogismo Disjuntivo
    "{" + i18n.phi + "," + i18n.connectors_sin[0] + i18n.phi + "}" + i18n.inference + i18n.psi, // Duns Scotus
    "{" + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[2] + i18n.theta + i18n.parenthesis[1] + "," + i18n.parenthesis[0] + i18n.psi + i18n.connectors_bin[2] + i18n.theta + i18n.parenthesis[1] + "," + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[1] + i18n.psi + i18n.parenthesis[1] + "}" + i18n.inference + i18n.theta, // Prova por casos
    "{" + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[0] + i18n.psi + i18n.parenthesis[1] + "}" + i18n.inference + i18n.phi, // Eliminação do segundo conjuntivo
    "{" + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[0] + i18n.psi + i18n.parenthesis[1] + "}" + i18n.inference + i18n.psi, // Eliminação do primeiro conjuntivo
    "{" + i18n.phi + "}" + i18n.inference + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[1] + i18n.psi + i18n.parenthesis[1], // Introdução do segundo disjuntivo
    "{" + i18n.psi + "}" + i18n.inference + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[1] + i18n.psi + i18n.parenthesis[1], // Introdução do primeiro disjuntivo
    "{" + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[2] + i18n.psi + i18n.parenthesis[1] + "," + i18n.parenthesis[0] + i18n.phi + i18n.connectors_bin[2] + i18n.connectors_sin[0] + i18n.psi + i18n.parenthesis[1] + "}" + i18n.inference + i18n.connectors_sin[0] + i18n.phi, // Lei de Redução ao Absurdo
]
export var inference_generated = 0
export var connector
export var inverse_connector

function randomInference()
{
    inference_generated = Math.floor(Math.random() * inferencies.length)
    return inferencies[inference_generated]
}

export function generateInference(complexity, difficulty)
{
    first_valoration = []
    second_valoration = []
    third_valoration = []
    var first_formula = gen_formula.generateFormula(complexity, difficulty)
    truth_table.evaluateTable()
    for (var i = 0; i < truth_table.valoration.length; i++)
    {
        first_valoration.push([...truth_table.valoration[i]])
    }
    var second_formula = gen_formula.generateFormula(complexity, difficulty)
    truth_table.evaluateTable()
    for (var i = 0; i < truth_table.valoration.length; i++)
    {
        second_valoration.push([...truth_table.valoration[i]])
    }
    var third_formula = gen_formula.generateFormula(complexity, difficulty)
    truth_table.evaluateTable()
    for (var i = 0; i < truth_table.valoration.length; i++)
    {
        third_valoration.push([...truth_table.valoration[i]])
    }

    var inference_aux = randomInference()

    return sprintf(inference_aux, first_formula, second_formula, third_formula, null, null)
}

export function generatePossibleInference(complexity, difficulty)
{
    var size = 2+Math.floor(Math.random()*2)
    var complexity_array = new Array()
    for (var i = 0; i <= size; i++)
    {
        complexity_array.push([1,1])    
    }
    gen_set.generateSet(size,complexity_array)

    var inference_aux = ""

    for(var i = 0; i < gen_set.set.length; i++)
    {
        if(i == gen_set.set.length-1)
        {
            inference_aux += i18n.horizontal_line
        }
        inference_aux += gen_set.set[i] + i18n.break_line
        
    }

    return inference_aux
}