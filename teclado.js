import * as i18n from "./i18n/arvore_decomp.js"

var focus

function addValue(element)
{
    if(focus)
    {
        document.getElementById(focus).value += element.target.innerText
        document.getElementById(focus).focus()
    }
}

function removeValue(element)
{
    if(focus)
    {
        document.getElementById(focus).value = document.getElementById(focus).value.slice(0,document.getElementById(focus).value.length-1)
        document.getElementById(focus).focus()
    }
}

export function createTecladoButtons()
{
    var teclado_div = document.getElementById("teclado")
    var teclado_element = document.createElement("div")
    teclado_element.id = "negacao"
    teclado_element.innerText = i18n.connectors_sin[0]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = "conjuncao"
    teclado_element.innerText = i18n.connectors_bin[0]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = "disjuncao"
    teclado_element.innerText = i18n.connectors_bin[1]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = "implicacao"
    teclado_element.innerText = i18n.connectors_bin[2]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = "equivalencia"
    teclado_element.innerText = i18n.connectors_bin[3]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    var teclado_br = document.createElement("br")
    teclado_div.append(teclado_br)
    teclado_element = document.createElement("div")
    teclado_element.id = "A"
    teclado_element.innerText = i18n.letters[0]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = "B"
    teclado_element.innerText = i18n.letters[1]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = "C"
    teclado_element.innerText = i18n.letters[2]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = "D"
    teclado_element.innerText = i18n.letters[3]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = "E"
    teclado_element.innerText = i18n.letters[4]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_br = document.createElement("br")
    teclado_div.append(teclado_br)
    teclado_element = document.createElement("div")
    teclado_element.id = "("
    teclado_element.innerText = i18n.parenthesis[0]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = ")"
    teclado_element.innerText = i18n.parenthesis[1]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = "V"
    teclado_element.innerText = i18n.tabela[0]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = "F"
    teclado_element.innerText = i18n.tabela[1]
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_br = document.createElement("br")
    teclado_div.append(teclado_br)
    teclado_element = document.createElement("div")
    teclado_element.id = "comma"
    teclado_element.innerText = i18n.comma
    teclado_element.addEventListener('click', addValue)
    teclado_div.append(teclado_element)
    teclado_element = document.createElement("div")
    teclado_element.id = "backspace"
    teclado_element.innerText = i18n.backspace
    teclado_element.addEventListener('click', removeValue)
    teclado_div.append(teclado_element)
}

export function getLatestFocus()
{
    var elements = document.getElementsByTagName("input")
    for(var element of elements)
    {
        if(element.type == "text")
        {
            element.onfocus = function () {
                focus = this.id; 
                this.style.border = "2px groove blue"
            }
            element.onblur = function () {
                this.style.border = "1px groove gray"
            }
        }
    }
}